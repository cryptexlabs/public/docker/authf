
# Authf

Forward authentication service. Works well with [Traefik](https://doc.traefik.io/traefik/middlewares/forwardauth/) forward authentication

## Documentation
- [Development Environment Configuration](./app)
- [Environment variables](./docs/environment-variables.md)

## Development

### Running

#### Docker
```shell
cd docker
cp docker-compose.override.elasticsearch.example.yml docker-compose.override.yml
docker-compose up
```

### Swagger docs

http://localhost:3000/docs/v1


## Important!
You need to generate a new public private key pair. Do not use the key pairs in this project.

### Generating a new RSA Keypair for use with this service

```shell
openssl genrsa -out mykey.pem 4096
openssl rsa -in mykey.pem -pubout -out pubkey.pem
```

#### Note
If you are using a symmetric encryption algorithm like `HS256` then the public and private key path will be the same.

### Supported encryption algorithms:
See https://www.npmjs.com/package/jsonwebtoken for documentation on supported encryption algorithms

## Supported authentication types
- Basic
- Samsung Legacy

### Basic
Used for generating a new access and refresh token via a username and password

Request payload:
```json
{
  "type": "basic",
  "data": {
    "username": "johndoe",
    "password": "abadpassword"
  }
}
```

### Refresh
Only used for refreshing a previously created access and refresh token pair

Request payload:
```json
{
  "type": "refresh",
  "data": {
    "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjZjY0NTZlYi0zOThlLTQzMTQtYTljMS01MzliMzk3ZmQ2YjgiLCJ0eXBlIjoicmVmcmVzaCJ9.Rv5pHpvhujl1DRYEO1GOFvPoAKYl7xCSX_xd3Fby-yVHtopPuJWjE0mp01dfCszMqZzOOUz_0Ke67xzBWkdwzfvhwz2X6mIUZqdd12F2omTKs6pUdss5I8IASNlUzCWwgW5zCzt5ILcFiBqESkCa5oXFkFjh-UPuskVl2ZF2IBXS9l-QHkyOWOPJojzW348m3s7n2_XO01ESb7svC5mFnMA4Q86_vEm-sO0iUZ089LgKcIpm8KFehFSV6V_PcK35gk_wZxyJem7whOwjsaeUYMfitoDL7KC5KxzBw8QeZB3IF5BiFTp51cbsIPqKoVDYwGFuAv7VyNiiUx7qP5tGVg"
  }
}
```

## Supported database types
- Elasticsearch
- MySQL

## Concepts and Recommended Architecture

Authf does not know anything about users and is not a persistent store for user data. Authf is inteded to be an abstraction away from users. This enables the service to support authentication for multiple authentication types such as api keys, anonymous authentication as well as users.

### User authentication
Your user service should keep a record of the authentication ID tied to the users credentials. Ideally your user service also keeps track of when the authentication object associated with a user was last updated. This way if you update the grants in your token you will only call the `PUT /authentication/{authenticationId}` when something is actually different. If you, for example call save authentication REST API every time a user logs or refreshes their token, this will put unnecessary write pressure on your authentication database and will increase your costs. Since your client application typically tracks user ids, username, emails, etc it is better if during login for example that your app looks up the authentication Id and then redirects the request to the authentication service. The only service to service REST API call that is recommended is `PUT /authentication/{authenticationId}` as service to service API calls are not as reliable as a redirect and this endpoint must be secured in such a way that end-user/client applications are not able to create authentications without performing some other actions. You can either use some form of basic authentication on this endpoint or use network based security such as only exposing this route within your network but not through your API gateway.

### Authorization
This service is not intended to check authorization of any particular request but strictly authentication and token issuance, however we may be open to well conceived and flexibly designed proposals.

## Merge requests

Merge requests are welcome. Please open an issue before starting to work on a merge request. If you are working on a bug fix you can create the merge request before getting a thumbs up to add a feature. If you are adding a feature please wait for a thumbs up before creating a merge request. Please document and unit test your changes.

We would like to add support for more authentication types such as google, facebook, etc. We would also like to add support for more databases such as mongodb, aurora, postgres, etc.

## Code of Conduct

You will be treated with dignity and respect and your thoughts and ideas will not be ignored or brushed aside like most open source projects. Even if we do not ultimately agree with something you want to do we will give it thoughtful consideration and respond kindly.

## Support

Please open an issue if you have a question that may not be answered by the documentation.

## License

[MIT](./LICENSE.txt)