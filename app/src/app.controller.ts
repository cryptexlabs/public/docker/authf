import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  HttpCode,
  HttpStatus,
  Inject,
  Param,
  ParseBoolPipe,
  Patch,
  Post,
  Put,
  Query,
  UnauthorizedException,
} from "@nestjs/common";
import { Config } from "./config";
import {
  ApiBasicAuth,
  ApiBearerAuth,
  ApiBody,
  ApiHeader,
  ApiParam,
  ApiQuery,
  ApiResponse,
  ApiTags,
} from "@nestjs/swagger";
import { AuthenticationService } from "./authentication/authentication.service";
import {
  ApiMetaHeaders,
  ContextBuilder,
  ContextualHttpException,
  NotEmptyPipe,
  RestResponse,
  SimpleHttpResponse,
} from "@cryptexlabs/codex-nodejs-common";
import { LocalesEnum } from "./locale/enum";
import {
  ExampleApiAuth,
  ExampleAuthentication,
  exampleAuthenticationId,
  ExampleBasicAuth,
  ExampleRefreshAuth,
  ExampleSamsungAnyAuth,
  ExampleSamsungAuthTokenPairResponse,
  ExampleSamsungLegacyAuth,
  ExampleSamsungStatelessAuthenticateResponse,
  ExampleSimpleResponse,
} from "./example.data";
import {
  AuthenticateStatelessResponseDataInterface,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthfMetaTypeEnum,
  CreateTokenResponseDataInterface,
  CreateTokenWithAuthorizedAuthenticationInterface,
  JwtKeyResponseInterface,
  StatelessAuthenticateTypes,
} from "@cryptexlabs/authf-data-model";
import { JWK } from "node-jose";
import { TokenValidator } from "./token";
import { AuthenticationValidationPipe } from "./authentication/authentication.validation.pipe";
import { AdminAuthenticator } from "./authenticator/admin.authenticator";
import {
  ApiMetaHeadersInterface,
  MessageInterface,
} from "@cryptexlabs/codex-data-model";
import { appConfig } from "./setup";
import * as jwt from "jsonwebtoken";

@Controller(`${appConfig.appPrefix}/${appConfig.apiVersion}`)
@ApiMetaHeaders()
export class AppController {
  constructor(
    private readonly config: Config,
    private readonly authenticationService: AuthenticationService,
    private readonly tokenValidator: TokenValidator,
    @Inject("CONTEXT_BUILDER") private readonly contextBuilder: ContextBuilder,
    private readonly adminAuthenticator: AdminAuthenticator,
  ) {}

  @Get("key")
  @ApiResponse({
    schema: {
      example: {
        keys: [
          {
            alg: "RS256",
            kty: "RSA",
            use: "sig",
            x5c: ["MIIC+DCCAeC..."],
            n: "4niv4Zg...",
            e: "AQAB",
          },
        ],
      },
    },
  })
  public async getPublicKeys(): Promise<JwtKeyResponseInterface> {
    const key = await JWK.asKey(this.config.jwtPublicKey, "pem");
    const jwk: any = key.toJSON();

    return {
      keys: [
        {
          alg: this.config.jwtAlgorithm,
          kty: jwk.kty,
          use: "sig",
          n: jwk.n,
          e: jwk.e,
        },
      ],
    };
  }

  @Get("/blacklisted-subjects")
  @ApiResponse({
    schema: {
      example: ["4ea27c6e-57b1-4cf7-822b-f3db8171a8bd"],
    },
  })
  public async getBlacklistedSubjects(
    @Headers() headers: ApiMetaHeadersInterface,
  ) {
    const requestContext = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const blacklistedSubjects =
      await this.authenticationService.getBlacklistedSubjects(requestContext);

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      "com.cryptexlabs.authf.blacklisted-subjects",
      blacklistedSubjects,
    );
  }

  @Get("/authenticated")
  @ApiBearerAuth("access-token")
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
  })
  public async authenticated(@Headers() headers) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    if (headers["x-forwarded-method"] !== "OPTIONS") {
      const bearerToken = headers.authorization?.replace("Bearer ", "");

      await this.tokenValidator.validateAccessToken(context, bearerToken);

      const decodedAccessToken = jwt.decode(bearerToken) as unknown as any;

      const blacklistedSubjects =
        await this.authenticationService.getBlacklistedSubjects(context);
      if (blacklistedSubjects.includes(decodedAccessToken.sub)) {
        throw new UnauthorizedException();
      }
    }

    return new SimpleHttpResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.OK,
      LocalesEnum.SUCCESS,
    );
  }

  @Post("/authentication/:authenticationId/token")
  @ApiTags("authentication")
  @ApiParam({
    name: "authenticationId",
    type: String,
    required: true,
    example: exampleAuthenticationId,
  })
  @ApiBody({
    examples: {
      "Basic Auth": {
        value: new ExampleBasicAuth(),
        description: "Username and password authentication",
      },
      API: {
        value: new ExampleApiAuth(),
        description: "API key and secret",
      },
      Refresh: {
        value: new ExampleRefreshAuth(),
        description: "Create a new access token with a refresh token",
      },
      "Samsung Legacy": {
        value: new ExampleSamsungLegacyAuth(),
        description: "Login with samsung legacy authentication credentials",
      },
      "Create Token": {
        value: {
          authentication: {
            token: {
              expirationPolicy: {
                access: {
                  length: 900,
                },
                refresh: {
                  length: 2592000,
                },
              },
              subject: "4ea27c6e-57b1-4cf7-822b-f3db8171a8bd",
              body: {
                scopes: ["user:self:all"],
              },
            },
          },
        },
        description:
          "Create a token. Must set header 'X-Meta-Type: cryptexlabs.authf.token.create.authorized-authentication-with-verified' and use basic auth",
      },
    },
    schema: {},
  })
  @ApiResponse({
    schema: {
      example: new ExampleSamsungAuthTokenPairResponse(),
    },
    status: HttpStatus.OK,
  })
  @ApiHeader({
    name: "X-Meta-Type",
  })
  @ApiBasicAuth("admin")
  public async createToken(
    @Headers() headers,
    @Param("authenticationId") authenticationId: string,
    @Body(undefined)
    body: AuthenticateTypes | CreateTokenWithAuthorizedAuthenticationInterface,
  ): Promise<MessageInterface<CreateTokenResponseDataInterface>> {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    if (
      await this.authenticationService.isBlacklisted(context, authenticationId)
    ) {
      throw new UnauthorizedException();
    }

    const type = headers["x-meta-type"];

    let createTokenResponseDataInterface;
    if (!type) {
      const useBody = body as AuthenticateTypes;
      createTokenResponseDataInterface =
        await this.authenticationService.createTokenPairForUnverified(
          context,
          authenticationId,
          useBody,
        );
    } else {
      this.adminAuthenticator.validateBasicAuthenticationHeader(
        context,
        headers.authorization,
      );
      if (
        type ===
        "cryptexlabs.authf.token.create.authorized-authentication-with-unverified"
      ) {
        const useBody =
          body as CreateTokenWithAuthorizedAuthenticationInterface;
        createTokenResponseDataInterface =
          await this.authenticationService.createTokenPairForUnverifiedWithAuthorizedAuthenticationData(
            context,
            authenticationId,
            useBody.authenticate,
            useBody.authentication,
          );
      } else if (
        type ===
        "cryptexlabs.authf.token.create.authorized-authentication-with-verified"
      ) {
        const useBody =
          body as CreateTokenWithAuthorizedAuthenticationInterface;
        createTokenResponseDataInterface =
          await this.authenticationService.createTokenPairForVerifiedWithAuthorizedAuthenticationData(
            context,
            authenticationId,
            useBody.authentication,
          );
      } else {
        throw new ContextualHttpException(
          context,
          `Invalid header for x-meta-type`,
          HttpStatus.BAD_REQUEST,
        );
      }
    }

    return new RestResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.OK,
      AuthfMetaTypeEnum.AUTHENTICATION_TOKEN_PAIR,
      createTokenResponseDataInterface,
    );
  }

  @Post("/authentication/none/verification")
  @ApiTags("authentication")
  @ApiBody({
    examples: {
      Samsung: {
        value: new ExampleSamsungAnyAuth(),
        description:
          "Login with samsung credentials and information (stateless)",
      },
    },
    schema: {},
  })
  @ApiResponse({
    schema: {
      example: new ExampleSamsungStatelessAuthenticateResponse(),
    },
    status: HttpStatus.OK,
  })
  public async authenticateStateless(
    @Headers() headers,
    @Body(undefined)
    body: StatelessAuthenticateTypes,
  ): Promise<MessageInterface<AuthenticateStatelessResponseDataInterface>> {
    const requestContext = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    const data = await this.authenticationService.createStatelessVerification(
      requestContext,
      body,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new RestResponse(
      responseContext,
      HttpStatus.OK,
      AuthfMetaTypeEnum.AUTHENTICATION_VERIFICATION,
      data,
    );
  }

  @Patch("/authentication/:authenticationId")
  @ApiBasicAuth("admin")
  @ApiTags("authentication")
  @ApiParam({
    name: "authenticationId",
    type: String,
    required: true,
    example: exampleAuthenticationId,
  })
  @ApiBody({
    schema: {
      example: new ExampleAuthentication(),
    },
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.OK,
  })
  public async patchAuthentication(
    @Headers() headers,
    @Param("authenticationId") authenticationId: string,
    @Body(undefined, AuthenticationValidationPipe)
    body: AuthenticationInterface,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    this.adminAuthenticator.validateBasicAuthenticationHeader(
      context,
      headers.authorization,
    );

    await this.authenticationService.saveAuthentication(
      context,
      authenticationId,
      body,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.OK,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete("/authentication/:authenticationId/provider")
  @ApiBasicAuth("admin")
  @ApiTags("authentication")
  @ApiParam({
    name: "authenticationId",
    type: String,
    required: true,
    example: exampleAuthenticationId,
  })
  @ApiQuery({
    name: "type",
    required: true,
    enum: Object.values(AuthenticationProviderTypeEnum),
  })
  @ApiQuery({
    name: "identifier",
    required: true,
    example: "N8FCHG1RWGNWIUQKXGN5",
  })
  @ApiQuery({
    name: "deleteAuthenticationIfProvidersEmpty",
    required: false,
    example: "true",
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.OK,
  })
  @HttpCode(HttpStatus.OK)
  public async deleteAuthenticationProvider(
    @Param("authenticationId", NotEmptyPipe) authenticationId: string,
    @Query("type", NotEmptyPipe) type: AuthenticationProviderTypeEnum,
    @Query("identifier", NotEmptyPipe) identifier: string,
    @Query("deleteAuthenticationIfProvidersEmpty", ParseBoolPipe)
    deleteAuthenticationIfProvidersEmpty: boolean,
    @Headers() headers,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    this.adminAuthenticator.validateBasicAuthenticationHeader(
      context,
      headers.authorization,
    );

    await this.authenticationService.deleteAuthenticationProvider(
      context,
      authenticationId,
      type,
      identifier,
      deleteAuthenticationIfProvidersEmpty,
    );

    return new SimpleHttpResponse(
      this.contextBuilder
        .build()
        .setMetaFromHeadersForNewMessage(headers)
        .getResult(),
      HttpStatus.OK,
      LocalesEnum.SUCCESS,
    );
  }

  @Delete("/authentication/:authenticationId")
  @ApiBasicAuth("admin")
  @ApiTags("authentication")
  @ApiParam({
    name: "authenticationId",
    type: String,
    required: true,
    example: exampleAuthenticationId,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.OK,
  })
  public async deleteAuthentication(
    @Headers() headers,
    @Param("authenticationId") authenticationId: string,
  ) {
    const context = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    this.adminAuthenticator.validateBasicAuthenticationHeader(
      context,
      headers.authorization,
    );

    await this.authenticationService.deleteAuthentication(
      context,
      authenticationId,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.OK,
      LocalesEnum.SUCCESS,
    );
  }

  @Put("/authentication/:authenticationId/blacklisted")
  @ApiBasicAuth("admin")
  @ApiTags("authentication")
  @ApiParam({
    name: "authenticationId",
    type: String,
    required: true,
    example: exampleAuthenticationId,
  })
  @ApiResponse({
    schema: {
      example: new ExampleSimpleResponse(),
    },
    status: HttpStatus.OK,
  })
  @ApiBody({
    schema: {
      example: {
        blacklisted: true,
      },
    },
  })
  public async updateAuthenticationBlacklistedStatus(
    @Headers() headers: ApiMetaHeadersInterface & { authorization: string },
    @Param("authenticationId") authenticationId: string,
    @Body() body: { blacklisted: boolean },
  ): Promise<RestResponse<string>> {
    const requestContext = this.contextBuilder
      .build()
      .setMetaFromHeaders(headers)
      .getResult();

    this.adminAuthenticator.validateBasicAuthenticationHeader(
      requestContext,
      headers.authorization,
    );

    await this.authenticationService.updateAuthenticationBlacklistedStatus(
      requestContext,
      authenticationId,
      body.blacklisted,
    );

    const responseContext = this.contextBuilder
      .build()
      .setMetaFromHeadersForNewMessage(headers)
      .getResult();

    return new SimpleHttpResponse(
      responseContext,
      HttpStatus.OK,
      LocalesEnum.SUCCESS,
    );
  }
}
