import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from "@nestjs/common";
import { AppController } from "./app.controller";
import { APP_FILTER } from "@nestjs/core";
import {
  ContextBuilder,
  ForwardedUriMiddleware,
  HealthzService,
  ServiceClient,
} from "@cryptexlabs/codex-nodejs-common";
import { Config } from "./config";
import { Locale, MessageContext } from "@cryptexlabs/codex-data-model";
import { HealthzController } from "./healthz.controller";
import { AuthenticationService } from "./authentication/authentication.service";
import { i18nDir } from "./locale/locales";
import { appConfig, baseLogger, elasticsearchService } from "./setup";
import { AuthenticatorFactory } from "./authenticator/authenticator.factory";
import { TokenValidator } from "./token";
import { AuthenticationDataServiceFactory } from "./authentication/authentication-data-service.factory";
import { AdminAuthenticator } from "./authenticator/admin.authenticator";
import { databaseModules } from "./data/database.modules";
import { DatabaseTypeEnum } from "./data/database-type.enum";
import { AuthenticationMysqlDataService } from "./authentication/authentication.mysql.data.service";
import { SamsungService } from "./provider/samsung/samsung.service";
import { SamsungController } from "./provider/samsung/samsung.controller";
import { ProviderIdentityService } from "./identity/provider-identity.service";
import { ProviderIdentityDataServiceFactory } from "./identity/provider-identity.data.service.factory";
import { ProviderIdentityMysqlDataService } from "./identity/provider-identity.mysql.data.service";
import { OauthHandler } from "./oauth/oauth.handler";
import { ReauthController } from "./reauth/reauth.controller";
import { ReAuthService } from "./reauth/reauth.service";
import { ExtraService } from "./extra/extra.service";
import { ExtraCognitoService } from "./extra/extra.cognito.service";
import { OpenTelemetryModuleImport } from "./telemetry";
import { LoggerModule } from "nestjs-pino";
import { pinoLoggerConfig } from "./logger.config";
import { AuthenticationDataServiceInterface } from "./authentication/authentication.data.service.interface";
import { AuthenticationDataService } from "./authentication/authentication.data.service";
import { AuthenticationCacheService } from "./authentication/authentication.cache.service";
import { RedisModule } from "./redis.module";
import { HealthzRefreshService } from "./healthz.refresh.service";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { AppHttpExceptionFilter } from "./app.http.exception.filter";

const appModuleImports = [];

const defaultLocale = new Locale(
  appConfig.fallbackLanguage,
  appConfig.fallbackCountry,
);

if (appConfig.telemetry.metrics.enabled) {
  appModuleImports.push(OpenTelemetryModuleImport);
}

if (appConfig.loggerType.includes("pino")) {
  appModuleImports.push(
    LoggerModule.forRoot({
      ...pinoLoggerConfig(appConfig),
      exclude: [
        {
          method: RequestMethod.GET,
          path: "/healthz",
        },
        {
          method: RequestMethod.GET,
          path: appConfig.telemetry.metrics.path,
        },
      ],
    }),
  );
}

const appFilter = new AppHttpExceptionFilter(
  defaultLocale,
  baseLogger,
  appConfig,
);

const providers: any[] = [
  {
    provide: APP_FILTER,
    useValue: appFilter,
  },
  {
    provide: "LOGGER",
    useValue: baseLogger,
  },
  {
    provide: "CONFIG",
    useValue: appConfig,
  },
  {
    provide: Locale,
    useValue: defaultLocale,
  },
  {
    provide: Config,
    useValue: appConfig,
  },
  AuthenticationService,
  ProviderIdentityService,
  AuthenticatorFactory,
  TokenValidator,
  AuthenticationDataServiceFactory,
  ProviderIdentityDataServiceFactory,
  AdminAuthenticator,
  OauthHandler,
  ReAuthService,
  ExtraService,
  ExtraCognitoService,
  AuthenticationDataService,
  AuthenticationCacheService,
  HealthzService,
  HealthzRefreshService,
  {
    provide: "HEALTHZ_SERVICE",
    useExisting: HealthzService,
  },
  {
    provide: ElasticsearchService,
    useValue: elasticsearchService,
  },
  {
    provide: SamsungService,
    useClass: SamsungService,
  },
  {
    provide: "AUTHENTICATION_PERSISTENCE_SERVICE",
    useFactory: async (factory: AuthenticationDataServiceFactory) => {
      return await factory.getPersistenceService(appConfig.dbType);
    },
    inject: [AuthenticationDataServiceFactory],
  },
  {
    provide: "AUTHENTICATION_DATA_SERVICE",
    useFactory: async (
      persistenceService: AuthenticationDataServiceInterface,
      config: Config,
      dataService: AuthenticationDataService,
    ) => {
      if (config.redis.enabled) {
        return dataService;
      } else {
        return persistenceService;
      }
    },
    inject: [
      "AUTHENTICATION_PERSISTENCE_SERVICE",
      "CONFIG",
      AuthenticationDataService,
    ],
  },
  {
    provide: "PROVIDER_IDENTITY_DATA_SERVICE",
    useFactory: async (factory: ProviderIdentityDataServiceFactory) => {
      return await factory.getService(appConfig.dbType);
    },
    inject: [ProviderIdentityDataServiceFactory],
  },
  {
    provide: "CONTEXT_BUILDER",
    useValue: new ContextBuilder(
      baseLogger,
      appConfig,
      new ServiceClient(appConfig),
      new MessageContext("default", null),
    ).setI18nDir(i18nDir),
  },
];

if (appConfig.dbType === DatabaseTypeEnum.MYSQL) {
  providers.push({
    provide: "MYSQL_AUTHENTICATION_DATA_SERVICE",
    useClass: AuthenticationMysqlDataService,
  });
  providers.push({
    provide: "MYSQL_PROVIDER_IDENTITY_DATA_SERVICE",
    useClass: ProviderIdentityMysqlDataService,
  });
} else {
  providers.push({
    provide: "MYSQL_AUTHENTICATION_DATA_SERVICE",
    useValue: null,
  });
  providers.push({
    provide: "MYSQL_PROVIDER_IDENTITY_DATA_SERVICE",
    useValue: null,
  });
}

@Module({
  imports: [...appModuleImports, ...databaseModules, RedisModule],
  controllers: [
    AppController,
    SamsungController,
    HealthzController,
    ReauthController,
  ],
  providers,
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer): any {
    consumer.apply(ForwardedUriMiddleware).forRoutes("/");
    // consumer.apply(ApiHeadersValidationMiddleware).forRoutes("/");
  }
}
