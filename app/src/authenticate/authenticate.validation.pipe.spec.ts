import { AuthenticateValidationPipe } from "./authenticate.validation.pipe";

describe("AuthenticationProviderValidationPipe", () => {
  const authenticationProviderValidationPipe = new AuthenticateValidationPipe();

  it("Should validate a basic authentication provider", () => {
    const json = `
            {
              "data": {
                "username": "johndoe",
                "password": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
              },
              "type": "basic"
            }
        `;
    const body = JSON.parse(json);
    authenticationProviderValidationPipe.transform(body, null);
  });

  it("Should validate an api authentication provider", () => {
    const json = `
            {
              "data": {
                "apiKey": "2o4239249429djsvn9239",
                "secret": "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3"
              },
              "type": "api"
            }
        `;
    const body = JSON.parse(json);
    authenticationProviderValidationPipe.transform(body, null);
  });

  it("Should validate a refresh authentication provider", () => {
    const json = `
            {
              "data": {
                "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhMjdhN2FlMS03MzUwLTQ2ZTYtODdhZC0yYTlhY2YwZDQ1OGYiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTYyNTA4NTk1NH0.H2lQfmg5AqAJthMdw6X7IhDOGnMXdeFfp0Z6BiCygYQwxqP0exUaC3djXMg2MunZoZfMcI8oRBcjXTQO2H4D_-YiyoHpGaYXpY0l0rEwVIaSdaJFwbPNmArib0CHXwyb3XdXbIU-ImKa3HyM4zFHqAG0vVZPpNYsIdnDDm6izHuuZ8EPggf8dTSVwBBKI9NK8lx7dcM9mx_lN4f1Oc6r8rJCyYZmR23kDNNw3SVD3SZULXBcVKUljlsHEV1EDuF4kaEc8_QBxIT1Es_up4byNqxJgsmzcFjEcKOSTb_MPHLjhgY23P2xaEzLg_2qrgzzMa7rSweze6GdchROh212AGqy6Y8AOZwyIa4DJuIbTikvLsGh1Mra1Xa68tZL32e03jq-euHFZA16qQazmpdN_5aFW2B0bJs5IjMYCkwwgw6JkEh75JorAiW0x6qOUDFmkchO-c-0cH93SeYkHkdsmXvYdM-9NfX4If4v6RqhKUsiAOASFeyfBp6ZmWfoMe4KainOn7wjOxpRZUG1qOBHQQfwFO2qyBlQsPhOjWS1cFpow1wKISCIgBM8kChFUnXZRN95ltqsii2dDktZwhj1S6mCP2u8q_hXAAwsb0ui6QyjGYDjYcJN0B_FLTqGSNrDrj7zP4x3WFkbj8MoC7pBAEVibtT6FrVWNJ58YIVqoIo"
              },
              "type": "refresh"
            }
        `;
    const body = JSON.parse(json);
    authenticationProviderValidationPipe.transform(body, null);
  });

  it("Should invalidate an invalid provider", () => {
    const json = `
            {
              "data": {
                "token": "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhMjdhN2FlMS03MzUwLTQ2ZTYtODdhZC0yYTlhY2YwZDQ1OGYiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTYyNTA4NTk1NH0.H2lQfmg5AqAJthMdw6X7IhDOGnMXdeFfp0Z6BiCygYQwxqP0exUaC3djXMg2MunZoZfMcI8oRBcjXTQO2H4D_-YiyoHpGaYXpY0l0rEwVIaSdaJFwbPNmArib0CHXwyb3XdXbIU-ImKa3HyM4zFHqAG0vVZPpNYsIdnDDm6izHuuZ8EPggf8dTSVwBBKI9NK8lx7dcM9mx_lN4f1Oc6r8rJCyYZmR23kDNNw3SVD3SZULXBcVKUljlsHEV1EDuF4kaEc8_QBxIT1Es_up4byNqxJgsmzcFjEcKOSTb_MPHLjhgY23P2xaEzLg_2qrgzzMa7rSweze6GdchROh212AGqy6Y8AOZwyIa4DJuIbTikvLsGh1Mra1Xa68tZL32e03jq-euHFZA16qQazmpdN_5aFW2B0bJs5IjMYCkwwgw6JkEh75JorAiW0x6qOUDFmkchO-c-0cH93SeYkHkdsmXvYdM-9NfX4If4v6RqhKUsiAOASFeyfBp6ZmWfoMe4KainOn7wjOxpRZUG1qOBHQQfwFO2qyBlQsPhOjWS1cFpow1wKISCIgBM8kChFUnXZRN95ltqsii2dDktZwhj1S6mCP2u8q_hXAAwsb0ui6QyjGYDjYcJN0B_FLTqGSNrDrj7zP4x3WFkbj8MoC7pBAEVibtT6FrVWNJ58YIVqoIo"
              },
              "type": "asdf"
            }
        `;
    const body = JSON.parse(json);

    expect(() => {
      authenticationProviderValidationPipe.transform(body, null);
    }).toThrow();
  });
});
