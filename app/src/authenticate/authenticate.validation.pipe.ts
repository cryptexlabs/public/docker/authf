import {
  ArgumentMetadata,
  BadRequestException,
  Injectable,
  PipeTransform,
} from "@nestjs/common";
import * as Joi from "joi";
import { basicAuthAuthenticateDataSchema } from "../provider/basic/basic-auth.authenticate.data.schema";
import { refreshAuthenticateDataSchema } from "../refresh/refresh.authenticate.data.schema";
import { samsungLegacyAuthenticateDataSchema } from "../provider/samsung-legacy/samsung-legacy.authenticate.data.schema";
import { apiAuthAuthenticateDataSchema } from "../provider/api/api-auth.authenticate.data.schema";

@Injectable()
export class AuthenticateValidationPipe implements PipeTransform {
  private schema: Joi.AlternativesSchema;

  constructor() {
    this.schema = Joi.alternatives().try(
      basicAuthAuthenticateDataSchema,
      apiAuthAuthenticateDataSchema,
      refreshAuthenticateDataSchema,
      samsungLegacyAuthenticateDataSchema,
    );
  }

  transform(value: any, metadata: ArgumentMetadata): any {
    const { error } = this.schema.validate(value);
    if (error) {
      throw new BadRequestException(error.message);
    }
    return value;
  }
}
