import { Inject, Injectable } from "@nestjs/common";
import { DatabaseTypeEnum } from "../data/database-type.enum";
import { AuthenticationDataServiceInterface } from "./authentication.data.service.interface";
import { AuthenticationElasticsearchDataService } from "./authentication.elasticsearch.data.service";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { Config } from "../config";
import { CustomLogger } from "@cryptexlabs/codex-nodejs-common";
import { AuthenticationMysqlDataService } from "./authentication.mysql.data.service";
import { ElasticsearchPinger } from "@cryptexlabs/codex-nodejs-common/lib/src/service/elasticsearch/elasticsearch.pinger";

@Injectable()
export class AuthenticationDataServiceFactory {
  constructor(
    private readonly config: Config,
    @Inject("LOGGER") private readonly logger: CustomLogger,
    @Inject("MYSQL_AUTHENTICATION_DATA_SERVICE")
    private readonly mysqlDataService: AuthenticationMysqlDataService,
  ) {}

  public async getPersistenceService(
    databaseType: DatabaseTypeEnum,
  ): Promise<AuthenticationDataServiceInterface> {
    if (databaseType === DatabaseTypeEnum.ELASTICSEARCH) {
      return this._getElasticsearchDatabaseService();
    }
    if (databaseType === DatabaseTypeEnum.MYSQL) {
      return this.mysqlDataService;
    }

    throw new Error(
      `Invalid database type: ${databaseType} from env var 'DATABASE_TYPE'`,
    );
  }

  private async _getElasticsearchDatabaseService(): Promise<AuthenticationElasticsearchDataService> {
    const elasticsearchService = new ElasticsearchService({
      node: this.config.elasticsearch.url,
    });
    const elasticsearchPinger = new ElasticsearchPinger(
      elasticsearchService,
      this.logger,
    );

    const service = new AuthenticationElasticsearchDataService(
      elasticsearchService,
      elasticsearchPinger,
      this.logger,
    );

    await service.initializeIndex();

    return service;
  }
}
