import { Inject, Injectable } from "@nestjs/common";
import { AuthenticationDataServiceInterface } from "./authentication.data.service.interface";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from "@cryptexlabs/authf-data-model";
import { Context, NotImplementedError } from "@cryptexlabs/codex-nodejs-common";
import { RedisClientType } from "redis";
import { AuthenticationUtil } from "./authentication.util";
import * as NodeCache from "node-cache";

/*
The purpose of this class is to resolve eventual consistency related issues in production environments.
It is not needed for small use cases
 */
@Injectable()
export class AuthenticationCacheService
  implements AuthenticationDataServiceInterface
{
  private readonly AUTHENTICATION_PREFIX = "authf:authentication";
  private readonly AUTHENTICATION_TTL = 5;
  private readonly AUTHENTICATION_BLACKLIST_KEY =
    "authf:blacklist:authentications";
  private readonly AUTHENTICATION_BLACKLIST_TTL = 5;

  private readonly SUBJECT_BLACKLIST_KEY = "authf:blacklist:subjects";
  private readonly SUBJECT_BLACKLIST_TTL = 5;

  private _memoryCache: NodeCache;

  constructor(@Inject("REDIS") private readonly redis: RedisClientType) {
    this._memoryCache = new NodeCache();
  }

  async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean> {
    return NotImplementedError.throw();
  }

  async getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface | null> {
    const json = await this.redis.get(
      `${this.AUTHENTICATION_PREFIX}:${authenticationId}`,
    );
    if (!json) {
      return null;
    }

    return JSON.parse(json);
  }

  /**
   *
   * @param context
   * @param authenticationId
   * @param authentication Must be the complete authentication. Implemented as putAuthentication
   */
  async patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    const existingAuthentication = await this.getAuthentication(
      context,
      authenticationId,
    );
    let finalAuthentication = authentication;
    if (existingAuthentication) {
      finalAuthentication = AuthenticationUtil.patchAuthentication(
        existingAuthentication,
        authentication,
      );
    }
    await this.redis.set(
      `${this.AUTHENTICATION_PREFIX}:${authenticationId}`,
      JSON.stringify(finalAuthentication),
      { EX: this.AUTHENTICATION_TTL },
    );
  }

  async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void> {
    return NotImplementedError.throw();
  }

  async deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return NotImplementedError.throw();
  }

  async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean> {
    let blacklist = await this.getBlacklistedAuthenticationIds(context);
    if (blacklisted) {
      blacklist.push(authenticationId);
    } else {
      blacklist = blacklist.filter((item) => item !== authenticationId);
    }
    await this.redis.set(
      this.AUTHENTICATION_BLACKLIST_KEY,
      JSON.stringify(blacklist),
    );
    this._memoryCache.set(this.AUTHENTICATION_BLACKLIST_KEY, blacklist);
    return true;
  }

  async getBlacklistedAuthenticationIds(
    context: Context,
  ): Promise<string[] | null> {
    const memoryCacheList: string[] = this._memoryCache.get(
      this.AUTHENTICATION_BLACKLIST_KEY,
    );
    if (memoryCacheList) {
      return memoryCacheList;
    }

    const json = await this.redis.get(this.AUTHENTICATION_BLACKLIST_KEY);
    if (json) {
      const blacklist = JSON.parse(json);
      this._memoryCache.set(
        this.AUTHENTICATION_BLACKLIST_KEY,
        blacklist,
        this.AUTHENTICATION_BLACKLIST_TTL,
      );
      return blacklist;
    }

    return null;
  }

  public async getBlacklistedSubjects(
    context: Context,
  ): Promise<string[] | null> {
    const memoryCacheList: string[] = this._memoryCache.get(
      this.SUBJECT_BLACKLIST_KEY,
    );
    if (memoryCacheList) {
      return memoryCacheList;
    }

    const json = await this.redis.get(this.SUBJECT_BLACKLIST_KEY);
    if (json) {
      const blacklist = JSON.parse(json);
      this._memoryCache.set(
        this.SUBJECT_BLACKLIST_KEY,
        blacklist,
        this.SUBJECT_BLACKLIST_TTL,
      );
      return blacklist;
    }

    return null;
  }

  async saveAuthenticationBlacklist(
    context: Context,
    blacklist: string[],
  ): Promise<void> {
    await this.redis.set(
      this.AUTHENTICATION_BLACKLIST_KEY,
      JSON.stringify(blacklist),
      { EX: this.AUTHENTICATION_BLACKLIST_TTL },
    );
    this._memoryCache.set(this.AUTHENTICATION_BLACKLIST_KEY, blacklist);
  }

  async saveSubjectBlacklist(
    context: Context,
    blacklist: string[],
  ): Promise<void> {
    await this.redis.set(
      this.SUBJECT_BLACKLIST_KEY,
      JSON.stringify(blacklist),
      { EX: 5 },
    );
    this._memoryCache.set(this.SUBJECT_BLACKLIST_KEY, blacklist);
  }
}
