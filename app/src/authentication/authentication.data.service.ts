import { Inject, Injectable } from "@nestjs/common";
import { AuthenticationDataServiceInterface } from "./authentication.data.service.interface";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from "@cryptexlabs/authf-data-model";
import { Context } from "@cryptexlabs/codex-nodejs-common";
import { AuthenticationCacheService } from "./authentication.cache.service";
import auth from "basic-auth";
import { AuthenticationUtil } from "./authentication.util";

@Injectable()
export class AuthenticationDataService
  implements AuthenticationDataServiceInterface
{
  constructor(
    @Inject("AUTHENTICATION_PERSISTENCE_SERVICE")
    private readonly persistenceService: AuthenticationDataServiceInterface,
    private readonly authenticationCacheService: AuthenticationCacheService,
  ) {}

  public async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean> {
    return await this.persistenceService.deleteAuthenticationProvider(
      context,
      authenticationId,
      type,
      identifier,
      deleteAuthenticationIfProvidersEmpty,
    );
  }

  public async getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface | null> {
    const cachedAuthentication =
      await this.authenticationCacheService.getAuthentication(
        context,
        authenticationId,
      );
    if (cachedAuthentication) {
      context.logger.debug(
        `Got authentication ${authenticationId} from cache`,
        cachedAuthentication,
      );
      return cachedAuthentication;
    }

    return await this.persistenceService.getAuthentication(
      context,
      authenticationId,
    );
  }

  public async patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    setTimeout(async () => {
      await this.persistenceService
        .patchAuthentication(context, authenticationId, authentication)
        .then()
        .catch((e) => context.logger.error(e));
    });

    await this.authenticationCacheService.patchAuthentication(
      context,
      authenticationId,
      authentication,
    );
  }

  public async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void> {
    return await this.persistenceService.patchAuthenticationProvider(
      context,
      authenticationId,
      provider,
    );
  }

  public deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    return this.persistenceService.deleteAuthentication(
      context,
      authenticationId,
    );
  }

  public async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean> {
    await this.authenticationCacheService.updateAuthenticationBlacklistedStatus(
      context,
      authenticationId,
      blacklisted,
    );
    return await this.persistenceService.updateAuthenticationBlacklistedStatus(
      context,
      authenticationId,
      blacklisted,
    );
  }

  public async getBlacklistedAuthenticationIds(
    context: Context,
  ): Promise<string[]> {
    const cachedBlacklist =
      await this.authenticationCacheService.getBlacklistedAuthenticationIds(
        context,
      );
    if (cachedBlacklist !== null) {
      return cachedBlacklist;
    }

    const blacklist =
      await this.persistenceService.getBlacklistedAuthenticationIds(context);

    await this.authenticationCacheService.saveAuthenticationBlacklist(
      context,
      blacklist,
    );

    return blacklist;
  }

  public async getBlacklistedSubjects(context: Context): Promise<string[]> {
    const cachedBlacklist =
      await this.authenticationCacheService.getBlacklistedSubjects(context);
    if (cachedBlacklist !== null) {
      return cachedBlacklist;
    }

    const blacklist = await this.persistenceService.getBlacklistedSubjects(
      context,
    );

    await this.authenticationCacheService.saveSubjectBlacklist(
      context,
      blacklist,
    );

    return blacklist;
  }
}
