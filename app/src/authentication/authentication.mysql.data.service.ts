import { AuthenticationDataServiceInterface } from "./authentication.data.service.interface";
import { Context, CustomLogger } from "@cryptexlabs/codex-nodejs-common";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
} from "@cryptexlabs/authf-data-model";
import { InjectKnex } from "nestjs-knex";
import { Knex } from "knex";
import { Inject, Injectable, NotFoundException } from "@nestjs/common";
import { SqlConstants } from "../data/sql.constants";
import { AuthenticationUtil } from "./authentication.util";
import { IdentifiedProviderFactoryUtil } from "../provider/identified-provider.factory.util";

@Injectable()
export class AuthenticationMysqlDataService
  implements AuthenticationDataServiceInterface
{
  constructor(
    @InjectKnex(SqlConstants.READ_ONLY) private readonly knexRO: Knex,
    @InjectKnex() private readonly knex: Knex,
    @Inject("LOGGER") private readonly logger: CustomLogger,
  ) {}

  async getAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<AuthenticationInterface> {
    const result = await this.knexRO("authentication")
      .where("authentication_id", authenticationId)
      .limit(1);

    if (result.length > 0) {
      return result[0].data;
    } else {
      return null;
    }
  }

  async patchAuthentication(
    context: Context,
    authenticationId: string,
    authentication: AuthenticationInterface,
  ): Promise<void> {
    context.logger.debug(`Authentication: ${JSON.stringify(authentication)}`);

    const result = await this.knexRO("authentication")
      .where("authentication_id", authenticationId)
      .limit(1);

    if (result.length > 0) {
      const existingAuthentication = result[0].data;

      const finalAuthentication = AuthenticationUtil.patchAuthentication(
        existingAuthentication,
        authentication,
      );

      await this.knex("authentication").update({
        authentication_id: authenticationId,
        data: JSON.stringify(finalAuthentication),
      });
    } else {
      await this.knex("authentication")
        .insert({
          authentication_id: authenticationId,
          data: JSON.stringify(authentication),
        })
        .onConflict("authentication_id")
        .merge();
    }
  }

  public async healthz(): Promise<void> {
    // TODO
  }

  public async patchAuthenticationProvider(
    context: Context,
    authenticationId: string,
    provider: AuthenticationTypes,
  ): Promise<void> {
    const authentication = await this.getAuthentication(
      context,
      authenticationId,
    );
    if (!authentication) {
      throw new NotFoundException();
    }

    const patchedAuthentication = AuthenticationUtil.patchProvider(
      authentication,
      provider,
    );

    await this.patchAuthentication(
      context,
      authenticationId,
      patchedAuthentication,
    );
  }

  public async deleteAuthenticationProvider(
    context: Context,
    authenticationId: string,
    type: AuthenticationProviderTypeEnum,
    identifier: string,
    deleteAuthenticationIfProvidersEmpty: boolean,
  ): Promise<boolean> {
    const result = await this.knexRO("authentication")
      .where("authentication_id", authenticationId)
      .limit(1);

    if (result.length === 0) {
      return false;
    }

    const authentication = result[0].data;

    const newProviders = authentication.providers.filter((provider) => {
      const identifiedProvider =
        IdentifiedProviderFactoryUtil.getIdentifiedProvider(provider);
      return identifiedProvider.getIdentifier() !== identifier;
    });

    const newAuthentication = {
      ...authentication,
      providers: newProviders,
    };

    if (newProviders.length === 0 && deleteAuthenticationIfProvidersEmpty) {
      await this.knex("authentication")
        .where({
          authentication_id: authenticationId,
        })
        .del();
    } else {
      await this.knex("authentication").update({
        authentication_id: authenticationId,
        data: JSON.stringify(newAuthentication),
      });
    }
    return true;
  }

  public async deleteAuthentication(
    context: Context,
    authenticationId: string,
  ): Promise<boolean> {
    const results = await this.knex("authentication")
      .where({
        authentication_id: authenticationId,
      })
      .del();
    return results > 0;
  }

  public async updateAuthenticationBlacklistedStatus(
    context: Context,
    authenticationId: string,
    blacklisted: boolean,
  ): Promise<boolean> {
    const results = await this.knex("authentication")
      .where({
        authentication_id: authenticationId,
      })
      .update({
        blacklisted,
      });
    return results > 0;
  }

  public async getBlacklistedAuthenticationIds(
    context: Context,
  ): Promise<string[]> {
    const result = await this.knexRO("authentication").where(
      "blacklisted",
      true,
    );

    return result.map((row) => row.authentication_id);
  }

  getBlacklistedSubjects(context: Context): Promise<string[]> {
    throw new Error("Method not implemented.");
  }
}
