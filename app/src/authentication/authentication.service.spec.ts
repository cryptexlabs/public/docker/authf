import { AuthenticationService } from "./authentication.service";
import { appConfig, contextBuilder } from "../setup";
import { instance, mock, verify, when } from "ts-mockito";
import { AuthenticationDataServiceInterface } from "./authentication.data.service.interface";
import { AuthenticatorFactory } from "../authenticator/authenticator.factory";
import { TokenValidator } from "../token";
import {
  ApiAuthAuthenticateInterface,
  AuthenticateTypeEnum,
  AuthenticateTypes,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
  BasicAuthAuthenticateInterface,
  CreateTokenResponseDataInterface,
  RefreshAuthenticateInterface,
  StatelessAuthenticateTypes,
} from "@cryptexlabs/authf-data-model";
import { BasicAuthenticator } from "../provider/basic/basic.authenticator";
import * as jwt from "jsonwebtoken";
import { anObjectLike } from "@cryptexlabs/codex-nodejs-common-tools/lib";
import { ApiAuthenticator } from "../provider/api/api.authenticator";
import { ExtraService } from "../extra/extra.service";
import {
  ContextualHttpException,
  CustomLogger,
} from "@cryptexlabs/codex-nodejs-common";
import { Config } from "../config";
import { HttpStatus, NotFoundException } from "@nestjs/common";

describe("AuthenticationService", () => {
  const context = contextBuilder.build().getResult();
  let MockAuthenticationDataService: AuthenticationDataServiceInterface;
  let MockAuthenticatorFactory: AuthenticatorFactory;
  let MockExtraService: ExtraService;
  let MockTokenValidator: TokenValidator;
  let MockLogger: CustomLogger;
  let service: AuthenticationService;
  let MockConfig: Config;
  let config: Config;

  beforeEach(() => {
    MockAuthenticationDataService = mock<AuthenticationDataServiceInterface>();
    const mockedAuthenticationDataService = instance(
      MockAuthenticationDataService,
    );

    MockAuthenticatorFactory = mock(AuthenticatorFactory);
    const mockedAuthenticationFactory = instance(MockAuthenticatorFactory);

    MockTokenValidator = mock(TokenValidator);
    const mockedTokenValidator = instance(MockTokenValidator);

    MockExtraService = mock(ExtraService);
    const mockedExtraService = instance(MockExtraService);

    MockLogger = mock(CustomLogger);
    const mockedLogger = instance(MockLogger);

    MockConfig = mock(Config);
    config = instance(MockConfig);

    service = new AuthenticationService(
      appConfig,
      mockedLogger,
      mockedAuthenticationDataService,
      mockedAuthenticationFactory,
      mockedTokenValidator,
      mockedExtraService,
    );
  });

  describe("createTokenPairForUnverified", () => {
    it("Should create a new token pair with basic auth", async () => {
      const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
      const subject = "d265839a-4374-44f7-928f-69ea54310be9";

      const basicAuthAuthenticate: BasicAuthAuthenticateInterface = {
        data: {
          username: "johndoe",
          password: "12345678",
        },
        type: AuthenticateTypeEnum.BASIC,
      };

      const authentication: AuthenticationInterface = {
        providers: [
          {
            type: AuthenticationProviderTypeEnum.BASIC,
            data: {
              username: "johndoe",
              password:
                "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
            },
          },
        ],
        token: {
          expirationPolicy: {
            access: {
              length: 0,
            },
            refresh: {
              length: 0,
            },
          },
          subject,
          body: {
            scopes: ["some:thing"],
          },
        },
      };
      when(
        MockAuthenticationDataService.getAuthentication(
          context,
          authenticationId,
        ),
      ).thenResolve(authentication);
      when(
        MockAuthenticatorFactory.getAuthenticator(
          context,
          basicAuthAuthenticate,
        ),
      ).thenReturn(new BasicAuthenticator(basicAuthAuthenticate.data, config));

      const tokenPairResultData = await service.createTokenPairForUnverified(
        context,
        authenticationId,
        basicAuthAuthenticate,
      );

      const decodedAccessToken = jwt.decode(
        tokenPairResultData.token.access.token,
      ) as unknown as any;
      expect(decodedAccessToken.sub).toBe(subject);
      expect(decodedAccessToken.type).toBe("access");
      expect(decodedAccessToken.scopes).toStrictEqual(["some:thing"]);

      const decodedRefreshToken = jwt.decode(
        tokenPairResultData.token.refresh.token,
      ) as unknown as any;
      expect(decodedRefreshToken.sub).toBe(authenticationId);
      expect(decodedRefreshToken.type).toBe("refresh");
    });

    it("Should throw an error if the authentication id is not found", async () => {
      const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
      const subject = "d265839a-4374-44f7-928f-69ea54310be9";

      const basicAuthAuthenticate: BasicAuthAuthenticateInterface = {
        data: {
          username: "johndoe",
          password: "12345678",
        },
        type: AuthenticateTypeEnum.BASIC,
      };

      when(
        MockAuthenticationDataService.getAuthentication(
          context,
          authenticationId,
        ),
      ).thenResolve(null);
      when(
        MockAuthenticatorFactory.getAuthenticator(
          context,
          basicAuthAuthenticate,
        ),
      ).thenReturn(new BasicAuthenticator(basicAuthAuthenticate.data, config));

      await expect(async () => {
        await service.createTokenPairForUnverified(
          context,
          authenticationId,
          basicAuthAuthenticate,
        );
      }).rejects.toBeInstanceOf(NotFoundException);
    });
  });

  it("Should create a new token pair with api auth", async () => {
    const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
    const subject = "d265839a-4374-44f7-928f-69ea54310be9";

    const apiAuthAuthenticate: ApiAuthAuthenticateInterface = {
      data: {
        apiKey: "johndoe",
        secret: "12345678",
      },
      type: AuthenticateTypeEnum.API,
    };

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.API,
          data: {
            apiKey: "johndoe",
            secret:
              "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
          },
        },
      ],
      token: {
        expirationPolicy: {
          access: {
            length: 0,
          },
          refresh: {
            length: 0,
          },
        },
        subject,
        body: {
          scopes: ["some:thing"],
        },
      },
    };
    when(
      MockAuthenticationDataService.getAuthentication(
        context,
        authenticationId,
      ),
    ).thenResolve(authentication);
    when(
      MockAuthenticatorFactory.getAuthenticator(context, apiAuthAuthenticate),
    ).thenReturn(new ApiAuthenticator(apiAuthAuthenticate.data, config));

    const tokenPairResultData = await service.createTokenPairForUnverified(
      context,
      authenticationId,
      apiAuthAuthenticate,
    );

    const decodedAccessToken = jwt.decode(
      tokenPairResultData.token.access.token,
    ) as unknown as any;
    expect(decodedAccessToken.sub).toBe(subject);
    expect(decodedAccessToken.type).toBe("access");
    expect(decodedAccessToken.scopes).toStrictEqual(["some:thing"]);

    const decodedRefreshToken = jwt.decode(
      tokenPairResultData.token.refresh.token,
    ) as unknown as any;
    expect(decodedRefreshToken.sub).toBe(authenticationId);
    expect(decodedRefreshToken.type).toBe("refresh");
  });

  it("Should create a new token pair with refresh auth", async () => {
    const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
    const subject = "d265839a-4374-44f7-928f-69ea54310be9";

    const authenticationProvider: RefreshAuthenticateInterface = {
      data: {
        token: "asdf",
      },
      type: AuthenticateTypeEnum.REFRESH,
    };

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: "johndoe",
            password:
              "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
          },
        },
      ],
      token: {
        expirationPolicy: {
          access: {
            length: 0,
          },
          refresh: {
            length: 0,
          },
        },
        subject,
        body: {
          scopes: ["some:thing"],
        },
      },
    };
    when(
      MockTokenValidator.validateRefreshToken(
        context,
        authenticationProvider.data.token,
        authenticationId,
      ),
    ).thenResolve();
    when(
      MockAuthenticationDataService.getAuthentication(
        context,
        authenticationId,
      ),
    ).thenResolve(authentication);

    const tokenPairResultData = await service.createTokenPairForUnverified(
      context,
      authenticationId,
      authenticationProvider,
    );

    const decodedAccessToken = jwt.decode(
      tokenPairResultData.token.access.token,
    ) as unknown as any;
    expect(decodedAccessToken.sub).toBe(subject);
    expect(decodedAccessToken.type).toBe("access");
    expect(decodedAccessToken.scopes).toStrictEqual(["some:thing"]);

    const decodedRefreshToken = jwt.decode(
      tokenPairResultData.token.refresh.token,
    ) as unknown as any;
    expect(decodedRefreshToken.sub).toBe(authenticationId);
    expect(decodedRefreshToken.type).toBe("refresh");
  });

  it("Should create a new token pair with refresh auth and expired access token", async () => {
    const authenticationId = "a27a7ae1-7350-46e6-87ad-2a9acf0d458f";
    const subject = "4ea27c6e-57b1-4cf7-822b-f3db8171a8bd";

    const authenticationProvider: RefreshAuthenticateInterface = {
      data: {
        token:
          "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJhMjdhN2FlMS03MzUwLTQ2ZTYtODdhZC0yYTlhY2YwZDQ1OGYiLCJ0eXBlIjoicmVmcmVzaCIsImlhdCI6MTcyODk1MjA4NCwiZXhwIjoxNzMxNTQ0MDg0fQ.QO9o5w4SGA5Sn23svUri-E10Y6V2W1fj03_FMslYd-QFIBp7gMNX8IPqQ7y_AcB8cAjVD2Tc1mBotoKSDMEjVnAH4XWWBfky6MSPMZRVWYmXsCggeMKHZkU95eeiGF_8cu7VXaD_VLtVtAbcRAwVgkeGB6c4VxR9qf5L7hhGLtG37Zexizw7F4JL18VYyT8PIuOkGqSxeCLX5-kl2jkCOZPhn16nHgOHQMmpDUttjZqaaKZj_enA46ATsUXXs_iiiy0H0m04XVLFsvFGHtTn4vuW9SEC9D6beHcLRZjtJYKkFG_-DSGkcHgeJsIa8OyuajpMH_VuKWNSKxrgd5_XGrdNQ6vpCbvaqMUsCQGSearm0MNHAfXGaGPNNIpqA-qJyC8XPkaG-wqx2Civkh4tSueFZKfnZaQcvYesJjte4QIza-DqT-19B2urTVQKSGf6vWiVwYDQPgBNcjdr6lYF_Ym_DrMMr1Fq-Sr4hPog_dy5-N5S-WJsmIRa8GV2EDB-Ouj6qua9a60VIjf8JI2jdiaKk4b86Cuz04qIyKla4XvwkOl2bM1_sCAhOB7SSGFa_ehAOu7VkyTlQztcN4Ems1Bd1iM7lalQ_wy-sbTm8Zq1WAAOzLExGyxzQTeZzO5QIbHwvrA02zgXcT0QpF1z-Nk14f9PDdDyumVixlyJqFw",
        expiredAccessToken:
          "eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiI0ZWEyN2M2ZS01N2IxLTRjZjctODIyYi1mM2RiODE3MWE4YmQiLCJ0eXBlIjoiYWNjZXNzIiwic2NvcGVzIjpbInVzZXI6c2VsZjphbGwiXSwiaWF0IjoxNzI4OTUyMDg0LCJleHAiOjE3Mjg5NTI5ODR9.BBt_zgLl0PgkAZPfqxTMI4eFibNKgiw6FjiNmwbZB4RE1hgZVTpMzzyokonehg4BipOCAiByf_-Pb-43mC9cc6aV8YUAiWzxKj8apHE_jyTV-rFIVR_89fWcDPwW_I8CAeT8UJP9RRjDwUggcg8QPUh7UWp14cEnJq5FmZGWIxfTg_3Sqx_JX7k3PhhKT2kfPEN4VV-KBTVVg4HSjnhSTzTcjVd2E0zfyJQFr0Ub0aBSbQZDHHzhdKmya4EAP3ZL39PIEBnC5y0uPP1wLqb_0a2zD0fqn9cD7qkI3_yNWokNegHD-YSKEeuquJWIOc4RbeXVWLTaVuCThkvfOGM6BX7LSgvqVGMmxOPrYWEliDsuGxIYEkezQcR8HHqwr-DamzTqpZUgj38xBQzOhZqpFbTk2DowfUaTl9zZ04zPy0a0RzeOcQ77--SViHkXbrAvGE886cGQmHveUkrkCJ1f0TvWGi0Ym7NaBGYcPClZ3nz0tpdk9U_4VwyIth0nQSgGwRCJkg5Fh8WFMcWSSq95awCEUspnHq8-0ZWOScoac6-uHs-p-5pnEdoIdhAhxAKWTHTJKno24S_1KToNlyFsQ1SWv822zUgcw9DF4v8p3uZuxP14_PHMwdiBKM4iUILTcc8oMHIJ7Ng9BTcG150uTtUs4fpeYUZJZMq9apk7lL4",
      },
      type: AuthenticateTypeEnum.REFRESH,
    };

    when(
      MockTokenValidator.validateRefreshToken(
        context,
        authenticationProvider.data.token,
        authenticationId,
      ),
    ).thenResolve();

    when(
      MockTokenValidator.validateRefreshToken(
        context,
        authenticationProvider.data.expiredAccessToken,
        authenticationId,
      ),
    ).thenResolve();

    const tokenPairResultData = await service.createTokenPairForUnverified(
      context,
      authenticationId,
      authenticationProvider,
    );

    const decodedAccessToken = jwt.decode(
      tokenPairResultData.token.access.token,
    ) as unknown as any;
    expect(decodedAccessToken.sub).toBe(subject);
    expect(decodedAccessToken.type).toBe("access");
    expect(decodedAccessToken.scopes).toStrictEqual(["user:self:all"]);

    const decodedRefreshToken = jwt.decode(
      tokenPairResultData.token.refresh.token,
    ) as unknown as any;
    expect(decodedRefreshToken.sub).toBe(authenticationId);
    expect(decodedRefreshToken.type).toBe("refresh");
  });

  describe("saveAuthentication", () => {
    it("Should save an authentication", async () => {
      const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
      const subject = "d265839a-4374-44f7-928f-69ea54310be9";
      const authentication: AuthenticationInterface = {
        providers: [
          {
            type: AuthenticationProviderTypeEnum.BASIC,
            data: {
              username: "johndoe",
              password: "12345678",
            },
          },
          {
            type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
            data: {
              appId: "asdf",
              appSecret: "1234",
              userId: "johndoe",
              username: null,
            },
          },
          {
            type: AuthenticationProviderTypeEnum.API,
            data: {
              apiKey: "johndoe",
              secret: "12345678",
            },
          },
        ],
        token: {
          expirationPolicy: {
            access: {
              length: 0,
            },
            refresh: {
              length: 0,
            },
          },
          subject,
          body: {
            scopes: ["some:thing"],
          },
        },
      };

      const authenticationToSave = {
        ...authentication,
        providers: [
          {
            ...authentication.providers[0],
            data: {
              ...authentication.providers[0].data,
              password:
                "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
            },
          },
          {
            ...authentication.providers[1],
          },
          {
            ...authentication.providers[2],
            data: {
              ...authentication.providers[2].data,
              secret:
                "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
            },
          },
        ],
      };

      when(
        MockAuthenticationDataService.getAuthentication(
          context,
          authenticationId,
        ),
      ).thenResolve(authentication);

      await service.saveAuthentication(
        context,
        authenticationId,
        authentication,
      );

      verify(
        MockAuthenticationDataService.patchAuthentication(
          context,
          authenticationId,
          anObjectLike(authenticationToSave),
        ),
      ).called();
    });

    it("Should save an authentication and handle replication lag", async () => {
      const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
      const subject = "d265839a-4374-44f7-928f-69ea54310be9";
      const authentication: AuthenticationInterface = {
        providers: [
          {
            type: AuthenticationProviderTypeEnum.BASIC,
            data: {
              username: "johndoe",
              password: "12345678",
            },
          },
          {
            type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
            data: {
              appId: "asdf",
              appSecret: "1234",
              userId: "johndoe",
              username: null,
            },
          },
          {
            type: AuthenticationProviderTypeEnum.API,
            data: {
              apiKey: "johndoe",
              secret: "12345678",
            },
          },
        ],
        token: {
          expirationPolicy: {
            access: {
              length: 0,
            },
            refresh: {
              length: 0,
            },
          },
          subject,
          body: {
            scopes: ["some:thing"],
          },
        },
      };

      const authenticationToSave = {
        ...authentication,
        providers: [
          {
            ...authentication.providers[0],
            data: {
              ...authentication.providers[0].data,
              password:
                "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
            },
          },
          {
            ...authentication.providers[1],
          },
          {
            ...authentication.providers[2],
            data: {
              ...authentication.providers[2].data,
              secret:
                "ef797c8118f02dfb649607dd5d3f8c7623048c9c063d532cc95c5ed7a898a64f",
            },
          },
        ],
      };

      when(
        MockAuthenticationDataService.patchAuthentication(
          context,
          authenticationId,
          anObjectLike(authenticationToSave),
        ),
      ).thenResolve(Promise.resolve() as any);

      let callCount = 0;
      when(
        MockAuthenticationDataService.getAuthentication(
          context,
          authenticationId,
        ),
      ).thenCall(() => {
        callCount += 1;
        if (callCount === 1) {
          return null;
        } else {
          return authentication;
        }
      });

      await service.saveAuthentication(
        context,
        authenticationId,
        authentication,
      );

      verify(
        MockAuthenticationDataService.patchAuthentication(
          context,
          authenticationId,
          anObjectLike(authenticationToSave),
        ),
      ).once();
      verify(
        MockAuthenticationDataService.getAuthentication(
          context,
          authenticationId,
        ),
      ).twice(); // Adjust this number based on your retry logic
    });
  });

  it("Should error if trying to save an invalid authentication", async () => {
    const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
    const subject = "d265839a-4374-44f7-928f-69ea54310be9";
    const authentication: any = {
      providers: [
        {
          type: AuthenticateTypeEnum.REFRESH,
          data: {
            token: "",
          },
        },
      ],
      token: {
        expirationPolicy: {
          access: {
            length: 0,
          },
          refresh: {
            length: 0,
          },
        },
        subject,
        body: {
          scopes: ["some:thing"],
        },
      },
    };

    await expect(
      service.saveAuthentication(context, authenticationId, authentication),
    ).rejects.toBeInstanceOf(Error);
  });

  it("Should error if trying to delete invalid authentication", async () => {
    const authenticationId = "dfe50389-a785-4ee1-90b5-c3deadcc6ed8";
    when(
      MockAuthenticationDataService.deleteAuthentication(
        context,
        authenticationId,
      ),
    ).thenResolve(false);
    await expect(
      service.deleteAuthentication(context, authenticationId),
    ).rejects.toBeInstanceOf(Error);
  });

  describe("createTokenPairForUnverifiedWithAuthorizedAuthenticationData", () => {
    it("should throw NotFoundException if authentication is not found", async () => {
      when(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).thenResolve(null);

      await expect(
        service.createTokenPairForUnverifiedWithAuthorizedAuthenticationData(
          context,
          "authId",
          { type: AuthenticateTypeEnum.BASIC } as AuthenticateTypes,
          {} as AuthenticationInterface,
        ),
      ).rejects.toBeInstanceOf(NotFoundException);

      verify(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).once();
    });

    it("should call createTokenPairForVerified if authentication is found", async () => {
      const authentication: AuthenticationInterface = { id: "authId" } as any;
      const authenticate: AuthenticateTypes = {
        type: AuthenticateTypeEnum.BASIC,
      } as any;
      const authorizedAuthentication: AuthenticationInterface = {
        id: "authId",
      } as any;

      when(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).thenResolve(authentication);
      const AuthenticatorMock = mock<any>(); // Replace with actual Authenticator interface
      when(
        MockAuthenticatorFactory.getAuthenticator(context, authenticate),
      ).thenReturn(instance(AuthenticatorMock));
      when(AuthenticatorMock.authenticate(context, authentication)).thenResolve(
        { id: "userId" } as any,
      );

      const createTokenPairForVerifiedSpy = jest
        .spyOn(service, "createTokenPairForVerified")
        .mockResolvedValue({} as CreateTokenResponseDataInterface);

      const result =
        await service.createTokenPairForUnverifiedWithAuthorizedAuthenticationData(
          context,
          "authId",
          authenticate,
          authorizedAuthentication,
        );

      expect(createTokenPairForVerifiedSpy).toHaveBeenCalledWith(
        context,
        "authId",
        authorizedAuthentication,
        { id: "userId" },
      );
      createTokenPairForVerifiedSpy.mockRestore();

      verify(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).once();
      verify(
        MockAuthenticatorFactory.getAuthenticator(context, authenticate),
      ).once();
      verify(AuthenticatorMock.authenticate(context, authentication)).once();
    });

    it("should call createTokenPairForUnverified if authenticate type is REFRESH", async () => {
      const authenticate: AuthenticateTypes = {
        type: AuthenticateTypeEnum.REFRESH,
      } as any;
      const authorizedAuthentication: AuthenticationInterface = {
        id: "authId",
      } as any;

      const createTokenPairForUnverifiedSpy = jest
        .spyOn(service, "createTokenPairForUnverified")
        .mockResolvedValue({} as CreateTokenResponseDataInterface);

      const result =
        await service.createTokenPairForUnverifiedWithAuthorizedAuthenticationData(
          context,
          "authId",
          authenticate,
          authorizedAuthentication,
        );

      expect(createTokenPairForUnverifiedSpy).toHaveBeenCalledWith(
        context,
        "authId",
        authenticate,
      );
      createTokenPairForUnverifiedSpy.mockRestore();
    });
  });

  describe("getAuthentication", () => {
    it("should return authentication if found", async () => {
      const authentication = { id: "authId" };
      when(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).thenResolve(authentication as any);

      const result = await service.getAuthentication(context, "authId");

      expect(result).toEqual(authentication);
      verify(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).once();
    });

    it("should throw NotFoundException if authentication is not found", async () => {
      when(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).thenResolve(null);

      await expect(
        service.getAuthentication(context, "authId"),
      ).rejects.toThrow(NotFoundException);

      verify(
        MockAuthenticationDataService.getAuthentication(context, "authId"),
      ).once();
    });
  });

  describe("deleteAuthenticationProvider", () => {
    it("should successfully delete the authentication provider if it exists", async () => {
      when(
        MockAuthenticationDataService.deleteAuthenticationProvider(
          context,
          "authId",
          AuthenticationProviderTypeEnum.BASIC,
          "identifier",
          true,
        ),
      ).thenResolve(true);

      await service.deleteAuthenticationProvider(
        context,
        "authId",
        AuthenticationProviderTypeEnum.BASIC,
        "identifier",
        true,
      );

      verify(
        MockAuthenticationDataService.deleteAuthenticationProvider(
          context,
          "authId",
          AuthenticationProviderTypeEnum.BASIC,
          "identifier",
          true,
        ),
      ).once();
    });

    it("should throw NotFoundException if the authentication provider does not exist", async () => {
      when(
        MockAuthenticationDataService.deleteAuthenticationProvider(
          context,
          "authId",
          AuthenticationProviderTypeEnum.BASIC,
          "identifier",
          true,
        ),
      ).thenResolve(false);

      await expect(
        service.deleteAuthenticationProvider(
          context,
          "authId",
          AuthenticationProviderTypeEnum.BASIC,
          "identifier",
          true,
        ),
      ).rejects.toThrow(NotFoundException);

      verify(
        MockAuthenticationDataService.deleteAuthenticationProvider(
          context,
          "authId",
          AuthenticationProviderTypeEnum.BASIC,
          "identifier",
          true,
        ),
      ).once();
    });
  });

  describe("updateAuthenticationBlacklistedStatus", () => {
    it("should successfully update the blacklisted status if the authentication ID exists", async () => {
      when(
        MockAuthenticationDataService.updateAuthenticationBlacklistedStatus(
          context,
          "authId",
          true,
        ),
      ).thenResolve(true);

      await service.updateAuthenticationBlacklistedStatus(
        context,
        "authId",
        true,
      );

      verify(
        MockAuthenticationDataService.updateAuthenticationBlacklistedStatus(
          context,
          "authId",
          true,
        ),
      ).once();
    });

    it("should throw NotFoundException if the authentication ID does not exist", async () => {
      when(
        MockAuthenticationDataService.updateAuthenticationBlacklistedStatus(
          context,
          "authId",
          true,
        ),
      ).thenResolve(false);

      await expect(
        service.updateAuthenticationBlacklistedStatus(context, "authId", true),
      ).rejects.toThrow(NotFoundException);

      verify(
        MockAuthenticationDataService.updateAuthenticationBlacklistedStatus(
          context,
          "authId",
          true,
        ),
      ).once();
    });
  });

  describe("patchAuthenticationProvider", () => {
    it("should patch the authentication provider successfully", async () => {
      const provider: AuthenticationTypes = {
        /* provider properties */
      } as any;
      const expectedResult = {
        /* expected result properties */
      };

      when(
        MockAuthenticationDataService.patchAuthenticationProvider(
          context,
          "authId",
          provider,
        ),
      ).thenResolve(expectedResult as any);

      const result = await service.patchAuthenticationProvider(
        context,
        "authId",
        provider,
      );

      expect(result).toEqual(expectedResult);
      verify(
        MockAuthenticationDataService.patchAuthenticationProvider(
          context,
          "authId",
          provider,
        ),
      ).once();
    });
  });

  describe("createStatelessVerification", () => {
    it("should throw ContextualHttpException if invalid authenticator type is provided", async () => {
      const authenticate: StatelessAuthenticateTypes = {
        type: "INVALID_TYPE",
      } as any;

      await expect(
        service.createStatelessVerification(context, authenticate),
      ).rejects.toMatchObject({
        constructor: ContextualHttpException,
        message: expect.stringContaining(`Invalid type: ${authenticate.type}`),
        status: HttpStatus.BAD_REQUEST,
      });
    });

    it("should call the authenticator's authenticate method if valid authenticator type is provided", async () => {
      const authenticate: StatelessAuthenticateTypes = {
        type: AuthenticateTypeEnum.SAMSUNG_ANY,
      } as any;
      const user = { id: "userId" };
      const authenticatorMock = mock<any>(); // Replace with actual Authenticator interface

      when(
        MockAuthenticatorFactory.getAuthenticator(
          context,
          authenticate as AuthenticateTypes,
        ),
      ).thenReturn(instance(authenticatorMock));
      when(authenticatorMock.authenticate(context)).thenResolve(user as any);

      const result = await service.createStatelessVerification(
        context,
        authenticate,
      );

      expect(result).toEqual({ user });
      verify(
        MockAuthenticatorFactory.getAuthenticator(
          context,
          authenticate as AuthenticateTypes,
        ),
      ).once();
      verify(authenticatorMock.authenticate(context)).once();
    });
  });

  describe("isBlacklisted", () => {
    it("should return true if the authentication ID is in the blacklisted list", async () => {
      const blacklistedList = ["authId1", "authId2"];
      const authenticationId = "authId1";

      when(
        MockAuthenticationDataService.getBlacklistedAuthenticationIds(context),
      ).thenResolve(blacklistedList);

      const result = await service.isBlacklisted(context, authenticationId);

      expect(result).toBe(true);
      verify(
        MockAuthenticationDataService.getBlacklistedAuthenticationIds(context),
      ).once();
    });

    it("should return false if the authentication ID is not in the blacklisted list", async () => {
      const blacklistedList = ["authId1", "authId2"];
      const authenticationId = "authId3";

      when(
        MockAuthenticationDataService.getBlacklistedAuthenticationIds(context),
      ).thenResolve(blacklistedList);

      const result = await service.isBlacklisted(context, authenticationId);

      expect(result).toBe(false);
      verify(
        MockAuthenticationDataService.getBlacklistedAuthenticationIds(context),
      ).once();
    });
  });

  describe("getBlacklistedSubjects", () => {
    it("should return the list of blacklisted subjects", async () => {
      const blacklistedSubjects = ["subject1", "subject2"];

      when(
        MockAuthenticationDataService.getBlacklistedSubjects(context),
      ).thenResolve(blacklistedSubjects);

      const result = await service.getBlacklistedSubjects(context);

      expect(result).toEqual(blacklistedSubjects);
      verify(
        MockAuthenticationDataService.getBlacklistedSubjects(context),
      ).once();
    });
  });

  describe("createTokenPairForVerifiedWithAuthorizedAuthenticationData", () => {
    it("should call createTokenPairForVerified with the correct parameters", async () => {
      const authenticationId = "authId";
      const authorizedAuthentication: AuthenticationInterface = {
        /* authorized authentication properties */
      } as any;
      const expectedResult: CreateTokenResponseDataInterface = {
        /* expected token response properties */
      } as any;

      // Mock the createTokenPairForVerified method to return expected result
      const createTokenPairForVerifiedSpy = jest
        .spyOn(service, "createTokenPairForVerified")
        .mockResolvedValue(expectedResult);

      const result =
        await service.createTokenPairForVerifiedWithAuthorizedAuthenticationData(
          context,
          authenticationId,
          authorizedAuthentication,
        );

      expect(result).toEqual(expectedResult);
      expect(createTokenPairForVerifiedSpy).toHaveBeenCalledWith(
        context,
        authenticationId,
        authorizedAuthentication,
        undefined,
      );

      createTokenPairForVerifiedSpy.mockRestore(); // Restore the original method
    });
  });
});
