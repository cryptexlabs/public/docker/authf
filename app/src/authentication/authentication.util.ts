import {
  AuthenticationInterface,
  AuthenticationTypes,
} from "@cryptexlabs/authf-data-model";
import * as _ from "lodash";
import { IdentifiedProviderFactoryUtil } from "../provider/identified-provider.factory.util";

export class AuthenticationUtil {
  public static patchProvider(
    authentication: AuthenticationInterface,
    provider: AuthenticationTypes,
  ): AuthenticationInterface {
    const newAuthentication = {
      ...authentication,
      providers: [...authentication.providers],
    };
    const identifiedProvider =
      IdentifiedProviderFactoryUtil.getIdentifiedProvider(provider);
    const existingProvider = newAuthentication.providers.find(
      (findProvider) => {
        const identifiedFindProvider =
          IdentifiedProviderFactoryUtil.getIdentifiedProvider(findProvider);
        return (
          identifiedFindProvider.getIdentifier() ===
            identifiedProvider.getIdentifier() &&
          identifiedFindProvider.provider.type ===
            identifiedProvider.provider.type
        );
      },
    );

    if (!existingProvider) {
      newAuthentication.providers.push(provider);
    } else {
      const newProviders = newAuthentication.providers.filter(
        (findProvider) => findProvider.type !== provider.type,
      );
      const newProvider = _.merge(existingProvider, provider);
      newProviders.push(newProvider);
      newAuthentication.providers = newProviders;
    }

    return newAuthentication;
  }
  public static patchAuthentication(
    existingAuthentication: AuthenticationInterface,
    newAuthentication: AuthenticationInterface,
  ): AuthenticationInterface {
    const finalProviders = [];

    // Add all existing providers not in new providers
    for (const existingProvider of existingAuthentication.providers) {
      const identifiedProvider =
        IdentifiedProviderFactoryUtil.getIdentifiedProvider(existingProvider);
      const newProvider = newAuthentication.providers.find((findProvider) => {
        const identifiedFindProvider =
          IdentifiedProviderFactoryUtil.getIdentifiedProvider(findProvider);
        return (
          identifiedFindProvider.getIdentifier() ===
            identifiedProvider.getIdentifier() &&
          identifiedFindProvider.provider.type ===
            identifiedProvider.provider.type
        );
      });
      // There is no new provider that matches. Just add it
      if (!newProvider) {
        finalProviders.push(existingProvider);
      }
    }

    // Add all new providers not in existing providers and merge new providers with existing ones
    for (const newProvider of newAuthentication.providers) {
      const identifiedProvider =
        IdentifiedProviderFactoryUtil.getIdentifiedProvider(newProvider);
      const existingProvider = existingAuthentication.providers.find(
        (findProvider) => {
          const identifiedFindProvider =
            IdentifiedProviderFactoryUtil.getIdentifiedProvider(findProvider);
          return (
            identifiedFindProvider.getIdentifier() ===
              identifiedProvider.getIdentifier() &&
            identifiedFindProvider.provider.type ===
              identifiedProvider.provider.type
          );
        },
      );
      // There is no existing provider that matches. Just add it
      if (!existingProvider) {
        finalProviders.push(newProvider);
      } else {
        const finalProvider = _.merge(existingProvider, newProvider);
        finalProviders.push(finalProvider);
      }
    }

    return {
      ...existingAuthentication,
      ...newAuthentication,
      providers: finalProviders,
    };
  }
}
