import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import * as basicAuth from "basic-auth";
import { Config } from "../config";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";

@Injectable()
export class AdminAuthenticator {
  constructor(private readonly config: Config) {}

  public validateBasicAuthenticationHeader(
    context: Context,
    authenticationHeader: string,
  ) {
    const authParts = basicAuth.parse(authenticationHeader);
    if (!authParts) {
      throw new ContextualHttpException(
        context,
        "Unauthorized",
        HttpStatus.UNAUTHORIZED,
      );
    }
    const { name, pass } = authParts;

    if (
      name !== this.config.adminUsername ||
      pass !== this.config.adminPassword
    ) {
      throw new ContextualHttpException(
        context,
        "Unauthorized",
        HttpStatus.UNAUTHORIZED,
      );
    }
  }
}
