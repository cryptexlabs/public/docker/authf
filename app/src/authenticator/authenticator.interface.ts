import {
  AuthenticationInterface,
  UserInterface,
} from "@cryptexlabs/authf-data-model";
import { Context } from "@cryptexlabs/codex-nodejs-common";

export interface AuthenticatorInterface {
  authenticate(
    context: Context,
    authentication?: AuthenticationInterface,
  ): Promise<UserInterface | null>;
}
