import {
  DefaultConfig,
  SqlConfigInterface,
} from "@cryptexlabs/codex-nodejs-common";
import * as fs from "fs";
import { Algorithm } from "jsonwebtoken";
import { appDir } from "./root";
import { DatabaseTypeEnum } from "./data/database-type.enum";
import {
  SamsungConfigInterface,
  SamsungOAuthClientConfigInterface,
} from "./server-config.interface";
import * as process from "process";

export class Config extends DefaultConfig {
  private _jwtPrivateKey;
  private _jwtPublicKey;
  private _http2PrivateKey;
  private _http2PublicKey;

  public get samsung(): SamsungConfigInterface {
    return this.getServerConfig().samsung;
  }

  public get samsungOAuthClients(): SamsungOAuthClientConfigInterface[] {
    const config = this.getServerConfig();
    if (config && config.oAuth && config.oAuth.samsung) {
      return config.oAuth.samsung;
    } else {
      return [];
    }
  }

  public get jwtPrivateKey(): Buffer {
    if (!this._jwtPrivateKey) {
      let path = process.env.JWT_PRIVATE_KEY_PATH;
      if (!path) {
        if (["test", "localhost", "ci"].includes(this.environmentName)) {
          path = `${appDir}/config/jwt-rs256.key`;
        } else {
          path = `/var/app/config/jwt-rs256.key`;
        }
      }
      this._jwtPrivateKey = fs.readFileSync(path);
    }
    return this._jwtPrivateKey;
  }

  public get jwtPublicKey(): Buffer {
    if (!this._jwtPublicKey) {
      let path = process.env.JWT_PUBLIC_KEY_PATH;
      if (!path) {
        if (["test", "localhost", "ci"].includes(this.environmentName)) {
          path = `${appDir}/config/jwt-rs256.pub`;
        } else {
          path = `/var/app/config/jwt-rs256.pub`;
        }
      }

      this._jwtPublicKey = fs.readFileSync(path);
    }
    return this._jwtPublicKey;
  }

  public get http2PrivateKey(): string {
    if (!this._http2PrivateKey) {
      let path = process.env.HTTP2_PRIVATE_KEY_PATH;
      if (!path) {
        if (["test", "localhost", "ci"].includes(this.environmentName)) {
          path = `${appDir}/config/http2.key`;
        } else {
          path = `/var/app/config/http2.key`;
        }
      }
      this._http2PrivateKey = fs.readFileSync(path, "utf-8");
    }
    return this._http2PrivateKey;
  }

  public get http2PublicKey(): string {
    if (!this._http2PublicKey) {
      let path = process.env.HTTP2_PUBLIC_KEY_PATH;
      if (!path) {
        if (["test", "localhost", "ci"].includes(this.environmentName)) {
          path = `${appDir}/config/http2.pub`;
        } else {
          path = `/var/app/config/http2.pub`;
        }
      }
      this._http2PublicKey = fs.readFileSync(path, "utf-8");
    }
    return this._http2PublicKey;
  }

  public get http2IdleConnectionTimeout(): number {
    return process.env.HTTP2_IDLE_CONNECTION_TIMEOUT
      ? parseInt(process.env.HTTP2_IDLE_CONNECTION_TIMEOUT, 10)
      : 60;
  }

  public get http2Hostname() {
    return process.env.HTTP2_HOSTNAME || "localhost";
  }

  public get jwtAlgorithm(): Algorithm {
    return (process.env.JWT_ALGORITHM as Algorithm) || "RS256";
  }

  public get dbType(): DatabaseTypeEnum {
    return (
      (process.env.DATABASE_TYPE as DatabaseTypeEnum) ||
      DatabaseTypeEnum.ELASTICSEARCH
    );
  }

  public get adminUsername(): string {
    return process.env.ADMIN_USERNAME || "admin";
  }

  public get adminPassword(): string {
    return process.env.ADMIN_PASSWORD || "password";
  }

  public get httpSecure(): boolean {
    return process.env.HTTP_SECURE === "true";
  }

  public get sql(): SqlConfigInterface {
    return {
      database: process.env.SQL_DATABASE,
      client: process.env.SQL_CLIENT,
      userName: process.env.SQL_USERNAME,
      password: process.env.SQL_PASSWORD,
      primary: {
        hostName: process.env.SQL_HOSTNAME,
        port: parseInt(process.env.SQL_PORT, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_MIN, 10),
          max: parseInt(process.env.SQL_POOL_MAX, 10),
        },
      },
      reader: {
        hostName: process.env.SQL_HOSTNAME_RO,
        port: parseInt(process.env.SQL_PORT_RO, 10),
        pool: {
          min: parseInt(process.env.SQL_POOL_RO_MIN, 10),
          max: parseInt(process.env.SQL_POOL_RO_MAX, 10),
        },
      },
    };
  }

  public get quietPinoReqLogger(): boolean {
    return process.env.LOGGER_PINO_QUIET_REQ_LOGGER === "true";
  }

  public get http2Port(): number {
    return process.env.HTTP2_PORT ? parseInt(process.env.HTTP2_PORT, 10) : 4000;
  }

  public get componentHealthRefreshInterval(): number {
    return process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS
      ? parseInt(process.env.COMPONENT_HEALTH_REFRESH_INTERVAL_IN_SECONDS, 10)
      : 60;
  }
}
