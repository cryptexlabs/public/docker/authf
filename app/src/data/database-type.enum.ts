export enum DatabaseTypeEnum {
  ELASTICSEARCH = "elasticsearch",
  MYSQL = "mysql",
}
