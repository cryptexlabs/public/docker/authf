import * as fs from "fs";
import * as jose from "node-jose";

const run = async () => {
  const keyStore = jose.JWK.createKeyStore();

  await keyStore.generate("RSA", 2048, { alg: "RS256", use: "sig" });

  fs.writeFileSync(
    "keys.json",
    JSON.stringify(keyStore.toJSON(true), undefined, 2),
  );
};

run().then();
