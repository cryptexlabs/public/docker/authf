import {
  Controller,
  Get,
  HttpStatus,
  Inject,
  InternalServerErrorException,
} from "@nestjs/common";
import {
  HealthzResponse,
  HealthzService,
} from "@cryptexlabs/codex-nodejs-common";
import { Locale } from "@cryptexlabs/codex-data-model";
import { Config } from "./config";

@Controller()
export class HealthzController {
  constructor(
    private readonly fallbackLocale: Locale,
    private readonly config: Config,
    @Inject("HEALTHZ_SERVICE") private readonly healthzService: HealthzService,
  ) {}

  @Get("healthz")
  public async root() {
    if (await this.healthzService.isHealthy()) {
      return new HealthzResponse(
        HttpStatus.OK,
        this.fallbackLocale,
        this.config,
      );
    } else {
      throw new InternalServerErrorException(`Service not healthy`);
    }
  }
}
