#!/usr/bin/env node

const axios = require("axios");
const jwt = require("jsonwebtoken");
const jwkToPem = require("jwk-to-pem");

async function start() {
  const baseUrl = "http://localhost:3007/api/v1";
  const authenticationId = "a27a7ae1-7350-46e6-87ad-2a9acf0d458f";
  const keysResponse = await axios.get(`${baseUrl}/key`);

  console.log(keysResponse.data);

  const tokenResponse = await axios.post(
    `${baseUrl}/authentication/${authenticationId}/token`,
    {
      data: {
        username: "johndoe",
        password:
          "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3",
      },
      type: "basic",
    },
    {
      headers: {
        "Accept-Language": "en-US",
      },
    },
  );

  const accessToken = tokenResponse.data.data.token.access.token;

  const jwk = keysResponse.data.keys[0];
  const pem = jwkToPem(jwk);

  try {
    const tokenPayload = jwt.verify(accessToken, pem, {
      algorithms: [jwk.alg],
    });
    console.log("Token is valid", tokenPayload);
  } catch (err) {
    console.error("Token validation failed", err);
  }
}

start().then();
