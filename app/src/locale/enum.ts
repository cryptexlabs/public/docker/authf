export enum LocalesEnum {
  STATUS = "status",
  ERROR = "error",
  SUCCESS = "success",
  UNKNOWN_ERROR = "unknown-error",
}
