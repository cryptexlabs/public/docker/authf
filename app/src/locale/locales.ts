import { I18n } from "i18n";
import * as packageJSON from "../../package.json";

const i18nData = {} as i18nAPI;

const i18nInstance = new I18n();

const i18nDir = __dirname;

i18nInstance.configure({
  locales: packageJSON.i18n.languages,
  directory: i18nDir,
  register: i18nData,
});

export { i18nData, i18nDir };
