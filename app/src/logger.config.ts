import { Options as PinoHttpOptions } from "pino-http";
import { v4 as uuidv4 } from "uuid";
import { Config } from "./config";
import pino from "pino";
import { DEFAULT_LOG_LEVELS } from "@cryptexlabs/codex-nodejs-common/lib/src/config/logging-config.interface";

export const PINO_DEFAULT_LEVELS = pino.levels.values;

export const pinoLoggerConfig = (
  appConfig: Config,
): { pinoHttp: PinoHttpOptions } => {
  return {
    pinoHttp: {
      transport:
        appConfig.loggerType === "pino-pretty"
          ? {
              target: "pino-pretty",
              options: {
                colorize: true,
                singleLine: true,
                messageKey: "message",
              },
            }
          : undefined,
      level: appConfig.logLevel,
      customLevels: DEFAULT_LOG_LEVELS,
      useLevel: "debug",
      redact: ["req.headers.authorization", "req.headers.cookie"], // redact any sensitive information in logs
      quietReqLogger: appConfig.quietPinoReqLogger,
      autoLogging: DEFAULT_LOG_LEVELS[appConfig.logLevel] < 30,
      genReqId: (req, res) => {
        const existingId = req.id ?? req.headers["x-request-id"];
        if (existingId) return existingId;
        const id = uuidv4();
        res.setHeader("x-request-id", id);
        return id;
      },
      messageKey: "message",
    },
  };
};
