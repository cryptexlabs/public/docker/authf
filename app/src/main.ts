import { Telemetry } from "./telemetry";
import { NestFactory } from "@nestjs/core";
import { AppModule } from "./app.module";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import * as packageJSON from "../package.json";
import { Config } from "./config";
import { appDir, baseDir } from "./root";
import { appConfig, baseLogger } from "./setup";
import { DatabaseTypeEnum } from "./data/database-type.enum";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { HealthzRefreshService } from "./healthz.refresh.service";
import { FastifyAdapter } from "@nestjs/platform-fastify";

const asciiText = `
    ___            __     __      ______
   /   |  __  __  / /_   / /_    / ____/
  / /| | / / / / / __/  / __ \\  / /_    
 / ___ |/ /_/ / / /_   / / / / / __/    
/_/  |_|\\__,_/  \\__/  /_/ /_/ /_/    

****************************************
*                                      *
*    FORWARD AUTHENTICATION SERVICE    *
*                                      *
****************************************              
    `;
async function bootstrap() {
  if (
    appConfig.telemetry.metrics.enabled ||
    appConfig.telemetry.traces.enabled
  ) {
    const telemetry = new Telemetry();
    await telemetry.start();
  }

  const config = new Config(baseDir, appDir, "localhost");

  const headers = [
    "X-Correlation-Id",
    "Accept-Language",
    "X-Started",
    "X-Context-Category",
    "X-Context-Id",
    "X-Client-Id",
    "X-Client-Name",
    "X-Client-Version",
    "X-Client-Variant",
    "X-Created",
    "Authorization",
    "Content-Type",
    "Accept",
    "Origin",
  ];

  const app = await NestFactory.create(AppModule, {
    cors: {
      origin: "*",
      methods: ["GET", "HEAD", "PUT", "PATCH", "POST", "DELETE", "OPTIONS"],
      allowedHeaders: headers,
      exposedHeaders: headers,
    },
  });

  const httpsOptions = {
    key: config.http2PrivateKey,
    cert: config.http2PublicKey,
  };

  const fastifyAdapter = new FastifyAdapter({
    http2: true,
    https: httpsOptions,
    http2SessionTimeout: config.http2IdleConnectionTimeout * 1000,
  } as any);

  const http2Server = await NestFactory.create(AppModule, fastifyAdapter);
  await http2Server.listen(config.http2Port, "0.0.0.0");

  const docOptions = new DocumentBuilder()
    .setTitle(packageJSON.name)
    .setDescription(packageJSON.description)
    .setVersion(packageJSON.version)
    .addBearerAuth(
      {
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT",
      },
      "access-token",
    )
    .addBasicAuth(
      {
        type: "http",
      },
      "admin",
    )
    .build();

  const document = SwaggerModule.createDocument(app, docOptions);
  const docsPath = `${config.docsPrefix}/${config.apiVersion}`;
  SwaggerModule.setup(
    `${config.docsPrefix}/${config.apiVersion}${config.docsPath}`,
    app,
    document,
  );

  const healthzRefresh = app.get<HealthzRefreshService>(HealthzRefreshService);
  healthzRefresh.startHealthzWatcher().then();

  app.enableShutdownHooks();

  await app.listen(config.httpPort, () => {
    console.log(asciiText);
    if (["docker", "localhost"].includes(appConfig.environmentName)) {
      const port = appConfig.externalHttpPort;
      console.log(`Swagger docs available on http://localhost:${port}/${config.docsPrefix}/${config.apiVersion}${config.docsPath}
      `);
    }
  });
}

const waitReady = async () => {
  if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
    await new Promise((resolve) => {
      const interval = setInterval(async () => {
        try {
          const elasticsearchService = new ElasticsearchService({
            node: appConfig.elasticsearch.url,
          });

          const ping = await elasticsearchService.ping();

          if (ping) {
            clearInterval(interval);
            resolve(undefined);
          }
        } catch (e) {
          baseLogger.debug("Elasticsearch still not healthy");
        }
      }, 1000);
    });
  }

  console.log(asciiText);

  process.exit(0);
};

const mode = process.env.MODE || "bootstrap";

const modeMap = {
  bootstrap,
  waitReady,
};

modeMap[mode]();
