import { Injectable } from "@nestjs/common";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  AuthenticationTypes,
  ReAuthAuthorizeDataInterface,
  UserInterface,
} from "@cryptexlabs/authf-data-model";
import { Context } from "@cryptexlabs/codex-nodejs-common";
import { v4 as uuidv4 } from "uuid";
import * as jwt from "jsonwebtoken";
import { appConfig } from "../setup";
import { ProviderIdentityService } from "../identity/provider-identity.service";
import { AuthenticationService } from "../authentication/authentication.service";

@Injectable()
export class OauthHandler {
  constructor(
    private readonly providerIdentityService: ProviderIdentityService,
    private readonly authenticationService: AuthenticationService,
  ) {}

  public async handleOauth(
    context: Context,
    res: any,
    req: any,
    user: UserInterface,
    provider: AuthenticationProviderTypeEnum,
    identityIdType: string,
    identityId: string,
    redirectUrl: string,
    finalRedirectUrl: string,
    authenticationProvider?: AuthenticationTypes,
  ): Promise<void> {
    let authenticationId =
      await this.providerIdentityService.getAuthenticationIdForProvider(
        AuthenticationProviderTypeEnum.SAMSUNG,
        identityIdType,
        identityId,
      );
    if (!authenticationId) {
      authenticationId = uuidv4();
      await this.providerIdentityService.saveProviderIdentity(
        provider,
        identityIdType,
        identityId,
        authenticationId,
      );
      const authentication: AuthenticationInterface = {
        providers: [authenticationProvider],
        token: null,
      };
      await this.authenticationService.saveAuthentication(
        context,
        authenticationId,
        authentication,
      );
      // Ensure authentication exists
      let notReady = true;
      do {
        try {
          await this.authenticationService.getAuthentication(
            context,
            authenticationId,
          );
          notReady = false;
        } catch (e) {
          context.logger.log(`Authentication ${authenticationId} not ready`);
          await new Promise((resolve) => setTimeout(resolve, 20));
        }
      } while (notReady);
    } else {
      if (authenticationProvider) {
        await this.authenticationService.patchAuthenticationProvider(
          context,
          authenticationId,
          authenticationProvider,
        );
      }
    }

    const jwtToken = jwt.sign(
      {
        sub: authenticationId,
        type: "access",
        scopes: [`authentication:${authenticationId}:any`],
      },
      appConfig.jwtPrivateKey,
      {
        algorithm: appConfig.jwtAlgorithm,
        expiresIn: 60,
      },
    );

    const thisHost = req.protocol + "://" + req.headers.host;

    const exampleResponseData: ReAuthAuthorizeDataInterface = {
      accessToken: jwtToken,
      user,
      authenticationId,
      redirectUrl: `${thisHost}/reauth/v1/create-token/${authenticationId}`,
      finalRedirectUrl,
    };

    const data = Buffer.from(JSON.stringify(exampleResponseData)).toString(
      "base64",
    );

    res.redirect(`${redirectUrl}?data=${data}`);
  }
}
