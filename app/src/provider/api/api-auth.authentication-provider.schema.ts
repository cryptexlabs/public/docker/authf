import * as Joi from "joi";
import { AuthenticationProviderTypeEnum } from "@cryptexlabs/authf-data-model";

export const apiAuthAuthenticationProviderSchema = Joi.object({
  type: Joi.string().valid(AuthenticationProviderTypeEnum.API).required(),
  data: Joi.object({
    apiKey: Joi.string().required(),
    secret: Joi.string().required(),
  }).required(),
});
