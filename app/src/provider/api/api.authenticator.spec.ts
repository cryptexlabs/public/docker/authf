import { ApiAuthenticator } from "./api.authenticator";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from "@cryptexlabs/authf-data-model";
import { HttpException, HttpStatus } from "@nestjs/common";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import { instance, mock } from "ts-mockito";
import { Config } from "../../config";

describe("ApiAuthenticator", () => {
  let MockConfig: Config;
  let config: Config;

  beforeEach(() => {
    MockConfig = mock(Config);
    config = instance(MockConfig);
  });

  it("Should match a apiKey and secret", async () => {
    const authenticator = new ApiAuthenticator(
      {
        apiKey: "johndoe",
        secret:
          "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3",
      },
      config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.API,
          data: {
            apiKey: "asdf",
            secret: "1234",
          },
        },
        {
          type: AuthenticationProviderTypeEnum.API,
          data: {
            apiKey: "johndoe",
            secret:
              "673654ebb29719a24810ec69eeaf5b2347bea9ef29648c02f6d214e31b5aad70",
          },
        },
      ],
      token: undefined,
    };

    const context = mock<Context>(Context);

    await authenticator.authenticate(context, authentication);
  });

  it("Should throw an exception if a apiKey and secret is not matched", async () => {
    const authenticator = new ApiAuthenticator(
      {
        apiKey: "johndoe",
        secret:
          "b9c950640e1b3740e98acb93e669c65766f6670dd1609ba91ff41052ba48c6f3",
      },
      config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.API,
          data: {
            apiKey: "johndoe",
            secret: "asdf",
          },
        },
      ],
      token: undefined,
    };

    const context = mock<Context>(Context);

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Unauthorized",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });
});
