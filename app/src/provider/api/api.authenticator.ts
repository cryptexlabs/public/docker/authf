import {
  ApiAuthAuthenticateDataInterface,
  ApiAuthAuthenticationProviderInterface,
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from "@cryptexlabs/authf-data-model";
import { HttpException, HttpStatus } from "@nestjs/common";
import * as crypto from "crypto";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import { AuthenticatorInterface } from "../../authenticator/authenticator.interface";
import { Config } from "../../config";

export class ApiAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: ApiAuthAuthenticateDataInterface,
    private readonly config: Config,
  ) {}

  async authenticate(
    context: Context,
    authentication: AuthenticationInterface,
  ): Promise<null> {
    const providers: ApiAuthAuthenticationProviderInterface[] =
      authentication.providers.filter((provider) => {
        return provider.type === AuthenticationProviderTypeEnum.API;
      }) as ApiAuthAuthenticationProviderInterface[];

    for (const provider of providers) {
      if (provider.data.apiKey === this.data.apiKey) {
        const expectedPassword = crypto
          .createHash("sha256")
          .update(this.data.secret)
          .digest("hex");
        if (expectedPassword === provider.data.secret) {
          return null;
        }
      }
    }
    throw new ContextualHttpException(
      context,
      "Unauthorized",
      HttpStatus.UNAUTHORIZED,
    );
  }
}
