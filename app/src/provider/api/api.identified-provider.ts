import { ApiAuthAuthenticationProviderInterface } from "@cryptexlabs/authf-data-model";
import { IdentifiedProviderInterface } from "../identified-provider.interface";

export class ApiIdentifiedProvider implements IdentifiedProviderInterface {
  constructor(
    public readonly provider: ApiAuthAuthenticationProviderInterface,
  ) {}

  getIdentifier(): string {
    return this.provider.data.apiKey;
  }
}
