import * as Joi from "joi";
import { AuthenticationProviderTypeEnum } from "@cryptexlabs/authf-data-model";

export const basicAuthAuthenticationProviderSchema = Joi.object({
  type: Joi.string().valid(AuthenticationProviderTypeEnum.BASIC).required(),
  data: Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
  }).required(),
});
