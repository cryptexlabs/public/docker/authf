import { BasicAuthAuthenticationProviderInterface } from "@cryptexlabs/authf-data-model";
import { IdentifiedProviderInterface } from "../identified-provider.interface";

export class BasicIdentifiedProvider implements IdentifiedProviderInterface {
  constructor(
    public readonly provider: BasicAuthAuthenticationProviderInterface,
  ) {}

  getIdentifier(): string {
    return this.provider.data.username;
  }
}
