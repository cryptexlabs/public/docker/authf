import { IdentifiedProviderInterface } from "./identified-provider.interface";
import {
  AuthenticationProviderInterface,
  AuthenticationProviderTypeEnum,
} from "@cryptexlabs/authf-data-model";
import { SamsungLegacyIdentifiedProvider } from "./samsung-legacy/samsung-legacy.identified-provider";
import { SamsungIdentifiedProvider } from "./samsung/samsung.identified-provider";
import { BasicIdentifiedProvider } from "./basic/basic.identified-provider";
import { ApiIdentifiedProvider } from "./api/api.identified-provider";

export class IdentifiedProviderFactoryUtil {
  static getIdentifiedProvider(
    provider: AuthenticationProviderInterface,
  ): IdentifiedProviderInterface {
    const map = {
      [AuthenticationProviderTypeEnum.SAMSUNG_LEGACY]:
        SamsungLegacyIdentifiedProvider,
      [AuthenticationProviderTypeEnum.SAMSUNG]: SamsungIdentifiedProvider,
      [AuthenticationProviderTypeEnum.BASIC]: BasicIdentifiedProvider,
      [AuthenticationProviderTypeEnum.API]: ApiIdentifiedProvider,
    };

    if (!map[provider.type]) {
      throw new Error(`Invalid identified provider: ${provider.type}`);
    }

    const clazz = map[provider.type];
    // noinspection TypeScriptValidateTypes
    return new clazz(provider);
  }
}
