import { AuthenticationProviderInterface } from "@cryptexlabs/authf-data-model";

export interface IdentifiedProviderInterface {
  getIdentifier(): string;
  provider: AuthenticationProviderInterface;
}
