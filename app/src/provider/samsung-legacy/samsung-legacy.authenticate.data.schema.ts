import * as Joi from "joi";
import { AuthenticationProviderTypeEnum } from "@cryptexlabs/authf-data-model";

export const samsungLegacyAuthenticateDataSchema = Joi.object({
  type: Joi.string()
    .valid(AuthenticationProviderTypeEnum.SAMSUNG_LEGACY)
    .required(),
  data: Joi.object({
    accessToken: Joi.string().required(),
    apiServerUrl: Joi.string().required(),
  }).required(),
});
