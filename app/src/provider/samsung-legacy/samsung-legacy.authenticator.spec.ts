import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
} from "@cryptexlabs/authf-data-model";
import { HttpException, HttpStatus } from "@nestjs/common";
import { SamsungLegacyAuthenticator } from "./samsung-legacy.authenticator";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import axios from "axios";
import * as fs from "fs";
import { Config } from "../../config";

jest.mock("axios");
const mockedAxios = axios as jest.Mocked<typeof axios>;

const fixtureDataPath = __dirname + "/fixture-data";

describe("SamsungAuthenticator", () => {
  let authorizeResponseInvalid;
  let authorizeResponseExpired;
  let authorizeResponseSuccess;
  let authorizeResponseBadXml;
  let authorizeResponseUnknownErrorCode;
  let userProfileResponseMultipleUserIdentification;
  let userProfileResponse;
  let userProfileWithPhoneResponse;
  let userProfileWithPhoneOnlyResponse;

  const context = {
    logger: {
      debug: () => {},
      error: () => {},
    },
  } as unknown as Context;

  beforeAll(() => {
    authorizeResponseInvalid = fs
      .readFileSync(
        `${fixtureDataPath}/authorize-response-invalid-access-token.xml`,
      )
      .toString();
    authorizeResponseExpired = fs
      .readFileSync(
        `${fixtureDataPath}/authorize-response-expired-access-token.xml`,
      )
      .toString();
    authorizeResponseUnknownErrorCode = fs
      .readFileSync(`${fixtureDataPath}/authorize-response-unknown-error.xml`)
      .toString();
    authorizeResponseSuccess = fs
      .readFileSync(`${fixtureDataPath}/authorize-response-success.xml`)
      .toString();
    userProfileResponseMultipleUserIdentification = fs
      .readFileSync(
        `${fixtureDataPath}/user-profile-response-multiple-user-identifications.xml`,
      )
      .toString();
    authorizeResponseBadXml = fs
      .readFileSync(`${fixtureDataPath}/authorize-response-bad-xml.xml`)
      .toString();
    userProfileResponse = fs
      .readFileSync(`${fixtureDataPath}/user-profile-response.xml`)
      .toString();
    userProfileWithPhoneResponse = fs
      .readFileSync(`${fixtureDataPath}/user-profile-response-with-phone.xml`)
      .toString();
    userProfileWithPhoneOnlyResponse = fs
      .readFileSync(
        `${fixtureDataPath}/user-profile-response-with-phone-only.xml`,
      )
      .toString();
  });

  it("Should authenticate", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.BASIC,
          data: {
            username: "asdf",
            password: "1234",
          },
        },
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockResolvedValue({
      data: authorizeResponseSuccess,
    } as any);
    mockedAxios.get.mockResolvedValue({ data: userProfileResponse } as any);

    await authenticator.authenticate(context as Context, authentication);
  });

  it("Should get profile with multiple user identifications", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockResolvedValue({
      data: authorizeResponseSuccess,
    } as any);
    mockedAxios.get.mockResolvedValue({
      data: userProfileResponseMultipleUserIdentification,
    } as any);

    await authenticator.authenticate(context as Context, authentication);
  });

  it("Should throw an exception if access token is expired", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue({
      response: {
        data: authorizeResponseExpired,
        status: HttpStatus.BAD_REQUEST,
      },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Token invalid or expired",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });

  it("Should throw an exception if access token is invalid for unknown reason", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue({
      response: {
        status: HttpStatus.UNAUTHORIZED,
        data: authorizeResponseExpired,
      },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Error authorizing access token",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });

  it("Should throw an exception if access token is invalid", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue({
      response: {
        status: HttpStatus.UNAUTHORIZED,
        data: authorizeResponseInvalid,
      },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Invalid access token",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });

  it("Should throw an exception if account is not found when authorizing token", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue({ code: "ENOTFOUND" });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Incorrect Account information provided",
        HttpStatus.BAD_REQUEST,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.BAD_REQUEST);
    }
  });

  it("Should throw an exception if some strange error occurs with authorize token", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue(new Error("walawala"));

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(new Error("walawala"));
  });

  it("Should throw an exception if authorize token returns bad xml", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockResolvedValue({
      data: authorizeResponseBadXml,
    } as any);

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Invalid authentication response data from Samsung Account.",
        HttpStatus.INTERNAL_SERVER_ERROR,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it("Should throw an exception if authorize token returns unknown error code for bad request", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.post.mockRejectedValue({
      response: {
        data: authorizeResponseUnknownErrorCode,
        status: HttpStatus.BAD_REQUEST,
      },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "The unknown error message",
        HttpStatus.INTERNAL_SERVER_ERROR,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it("Should throw an exception if authorize token returns internal server error", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: true,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    const exception = new ContextualHttpException(
      context,
      "Ooops",
      HttpStatus.INTERNAL_SERVER_ERROR,
    );

    // @ts-ignore
    exception.response = {
      data: "Blah blah blah",
      status: HttpStatus.INTERNAL_SERVER_ERROR,
    };
    mockedAxios.post.mockRejectedValue(exception);

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Ooops",
        HttpStatus.INTERNAL_SERVER_ERROR,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it("Should throw an exception if get profile returns bad xml", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: false,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.get.mockResolvedValue({ data: authorizeResponseBadXml } as any);

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Invalid profile data from Samsung Account",
        HttpStatus.INTERNAL_SERVER_ERROR,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.INTERNAL_SERVER_ERROR);
    }
  });

  it("Should throw an exception if get profile returns unauthorized", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: false,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.get.mockRejectedValue({
      response: { status: HttpStatus.UNAUTHORIZED },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Error getting samsung account user profile",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });

  it("Should throw an exception if get profile returns forbidden", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: false,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [
        {
          type: AuthenticationProviderTypeEnum.SAMSUNG_LEGACY,
          data: {
            appSecret: "123456",
            userId: "john",
            appId: "iviviv",
            username: null,
          },
        },
      ],
      token: undefined,
    };

    mockedAxios.get.mockRejectedValue({
      response: { status: HttpStatus.FORBIDDEN },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Error getting samsung account user profile",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });

  it("Should throw an exception if there are no authorized providers", async () => {
    const authenticator = new SamsungLegacyAuthenticator(
      {
        accessToken: "asdf",
        apiServerUrl: "asdf.com",
      },
      {
        samsung: {
          authorizeAccessToken: false,
        },
      } as Config,
    );

    const authentication: AuthenticationInterface = {
      providers: [],
      token: undefined,
    };

    mockedAxios.get.mockRejectedValue({
      response: { status: HttpStatus.UNAUTHORIZED },
    });

    await expect(
      authenticator.authenticate(context, authentication),
    ).rejects.toMatchObject(
      new ContextualHttpException(
        context,
        "Unauthorized",
        HttpStatus.UNAUTHORIZED,
      ),
    );
    try {
      await authenticator.authenticate(context, authentication);
    } catch (e) {
      expect(e.getStatus()).toBe(HttpStatus.UNAUTHORIZED);
    }
  });
});
