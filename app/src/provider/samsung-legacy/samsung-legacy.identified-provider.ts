import { SamsungLegacyAuthenticationProviderInterface } from "@cryptexlabs/authf-data-model";
import { IdentifiedProviderInterface } from "../identified-provider.interface";

export class SamsungLegacyIdentifiedProvider
  implements IdentifiedProviderInterface
{
  constructor(
    public readonly provider: SamsungLegacyAuthenticationProviderInterface,
  ) {}

  getIdentifier(): string {
    return this.provider.data.userId;
  }
}
