export enum SamsungAccessTokenTypeEnum {
  BEARER = "bearer",
}

export interface SamsungAccessTokenResponseDataInterface {
  /**
   * The access token
   */
  access_token: string;

  /**
   * The type of Authorization
   */
  token_type: SamsungAccessTokenTypeEnum;

  /**
   * A number representing the number of seconds until the token expires
   */
  access_token_expires_in: number;

  /**
   * A number representing the number of seconds until the token expires
   */
  expires_in: number;

  /**
   * The refresh token? or a string that says '-1'
   */
  refresh_token: string;

  /**
   * A number representing the number of seconds until the token expires or a string '-1'
   */
  refresh_token_expires_in: number | string;

  /**
   * The Samsung user ID, also known sometimes as SA GUID
   */
  userId: string;
}
