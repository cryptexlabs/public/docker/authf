import { Context } from "@cryptexlabs/codex-nodejs-common";
import { AuthenticatorInterface } from "../../authenticator/authenticator.interface";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  SamsungAnyAuthenticateDataInterface,
  SamsungAuthenticateInterface,
  SamsungAuthenticationProviderInterface,
  UserInterface,
} from "@cryptexlabs/authf-data-model";
import { Config } from "../../config";
import { SamsungApiUtil } from "./samsung.api.util";
import { HttpException, HttpStatus } from "@nestjs/common";

export class SamsungAnyAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: SamsungAnyAuthenticateDataInterface,
    private readonly config: Config,
  ) {}

  public async authenticate<T extends UserInterface | void = UserInterface>(
    context: Context,
  ): Promise<T> {
    return (await SamsungApiUtil.getUser(
      context,
      this.data.clientId,
      this.data.accessToken,
      this.data.userId,
      this.data.apiBaseUrl,
    )) as T;
  }
}
