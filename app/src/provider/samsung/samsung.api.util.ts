import { SamsungAccessTokenResponseDataInterface } from "./samsung.access-token.response-data.interface";
import { SamsungUserInfoResponseDataInterface } from "./samsung.user-info.response-data.interface";
import axios from "axios";
import { HttpStatus } from "@nestjs/common";
import { SamsungOAuthClientConfigInterface } from "../../server-config.interface";
import qs from "querystring";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import {
  EmailTypeEnum,
  PhoneTypeEnum,
  SamsungTokenUserInfoBaseUrlEnum,
  UserInterface,
} from "@cryptexlabs/authf-data-model";
import { SamsungDisplayName } from "./samsung.display-name";

export class SamsungApiUtil {
  private static async _getUserProfile(
    context: Context,
    clientId: string,
    accessToken: string,
    userId: string,
    apiBaseUrl?: SamsungTokenUserInfoBaseUrlEnum,
  ): Promise<SamsungUserInfoResponseDataInterface> {
    let useApiBaseUrl = "https://api.account.samsung.com";

    if (apiBaseUrl) {
      if (
        !Object.values(SamsungTokenUserInfoBaseUrlEnum).includes(apiBaseUrl)
      ) {
        throw new ContextualHttpException(
          context,
          `Invalid apiBaseUrl ${apiBaseUrl}`,
          HttpStatus.BAD_REQUEST,
        );
      }
      useApiBaseUrl = apiBaseUrl;
    }

    try {
      const result = await axios.get(
        `${useApiBaseUrl}/v2/profile/user/userinfo`,
        {
          headers: {
            Authorization: `Bearer ${accessToken}`,
            "x-osp-userId": userId,
            "x-osp-appId": clientId,
            Accept: "application/json",
          },
        },
      );
      return result.data;
    } catch (e) {
      if (!e.response) {
        if (
          [
            SamsungTokenUserInfoBaseUrlEnum.AP_STAGE,
            SamsungTokenUserInfoBaseUrlEnum.CN_STAGE,
          ].includes(apiBaseUrl as any)
        ) {
          throw new ContextualHttpException(
            context,
            `apiBaseUrl ${apiBaseUrl} not yet supported`,
            HttpStatus.NOT_IMPLEMENTED,
          );
        }
      }

      if (
        e.response?.status === HttpStatus.BAD_REQUEST ||
        e.response?.status === HttpStatus.NOT_FOUND ||
        e.response?.status === HttpStatus.FORBIDDEN ||
        e.response?.status === HttpStatus.UNAUTHORIZED
      ) {
        context.logger.verbose("sa error message", e.message);
        context.logger.verbose("sa response data", e.response?.data);
        context.logger.verbose("sa response headers", e.response?.headers);

        context.logger.verbose(
          `Got http status ${e.response?.status} when calling ${useApiBaseUrl}/v2/profile/user/userinfo with userId: ${userId} and clientId: ${clientId}`,
        );

        let extraMessage = "";
        if (apiBaseUrl) {
          extraMessage += ` Are you sure that apiBaseUrl=${apiBaseUrl} is the correct one?`;
        } else {
          extraMessage += ` You did not provide an apiBaseUrl. This might be the reason for the failure. `;
        }

        if (context.config.logLevels.includes("verbose")) {
          extraMessage +=
            " The apiBaseUrl should be the one that comes with the accessToken and userId.";
          extraMessage +=
            " According to the documentation this is also api_server_url. ";
          extraMessage +=
            " See https://document.account.samsung.com/oauth-web/ and/or https://document.account.samsung.com/oauth-android/";
          extraMessage += ` All possible apiBaseUrls are: ${Object.values(
            SamsungTokenUserInfoBaseUrlEnum,
          ).join(", ")}`;
        }

        throw new ContextualHttpException(
          context,
          `Invalid client ID, token and/or user ID.${extraMessage}`,
          HttpStatus.UNAUTHORIZED,
        );
      }
      context.logger.verbose("sa error message", e.message);
      context.logger.verbose("sa response data", e.response?.data);
      context.logger.verbose("sa response headers", e.response?.headers);

      context.logger.error(
        `Got http status ${e.response.status} when calling
         ${useApiBaseUrl}/v2/profile/user/userinfo with userId: ${userId} and clientId: ${clientId}`,
      );

      throw new ContextualHttpException(
        context,
        "Error getting user profile",
        HttpStatus.INTERNAL_SERVER_ERROR,
      );
    }
  }

  public static async getUser(
    context: Context,
    clientId: string,
    accessToken: string,
    userId: string,
    apiBaseUrl?: SamsungTokenUserInfoBaseUrlEnum,
  ): Promise<UserInterface> {
    const userProfile = await this._getUserProfile(
      context,
      clientId,
      accessToken,
      userId,
      apiBaseUrl,
    );

    return {
      username:
        "samsung:" + (userProfile.preferred_username || userProfile.email),
      profile: {
        name: {
          first: userProfile.given_name || null,
          middle: null,
          last: userProfile.family_name || null,
          display: new SamsungDisplayName(userProfile).toString(),
        },
        email: {
          other: [],
          primary: userProfile.email
            ? {
                value: userProfile.email,
                type: EmailTypeEnum.UNKNOWN,
              }
            : null,
        },
        phone: {
          other: [],
          primary: userProfile.phone_number
            ? {
                value: userProfile.phone_number,
                type: PhoneTypeEnum.UNKNOWN,
              }
            : null,
        },
        photo: {
          other: [],
          primary: userProfile.picture
            ? {
                url: userProfile.picture,
              }
            : null,
        },
      },
      timezone: null,
    };
  }

  public static async getAccessToken(
    context: Context,
    clientConfig: SamsungOAuthClientConfigInterface,
    code: string,
    redirectUri: string,
  ): Promise<SamsungAccessTokenResponseDataInterface> {
    const payload = {
      grant_type: "authorization_code",
      client_id: clientConfig.clientId,
      client_secret: clientConfig.clientSecret,
      code,
      redirect_uri: redirectUri,
    };

    try {
      const result = await axios.post(
        "https://api.account.samsung.com/auth/oauth2/v2/token",
        qs.stringify(payload),
        {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
            Accept: "text/json;charset=UTF-8",
          },
        },
      );

      return result.data;
    } catch (e) {
      if (e.response) {
        context.logger.error(e?.response?.data, e.trace);
        if (e.response.status) {
          if (e.response.status === HttpStatus.BAD_REQUEST) {
            const body = e.response.data;
            if (body.error && typeof e.error === "string") {
              context.logger.error(e.error, e.stack);
            }

            if (body.error_code === "AUT_1802") {
              // Yes we are changing the http status from 403 to 401
              throw new ContextualHttpException(
                context,
                "Unauthorized",
                HttpStatus.UNAUTHORIZED,
              );
            }

            throw new ContextualHttpException(
              context,
              "Unknown Error",
              HttpStatus.INTERNAL_SERVER_ERROR,
            );
          }

          // The token code is invalid
          if (e.response.status === HttpStatus.FORBIDDEN) {
            const body = e.response.data;
            if (body.error && typeof e.error === "string") {
              context.logger.error(e.error, e.stack);
            }

            if (body.error_code === "ACF_0403") {
              // This can happen if the request url is invalid. There is probably a bug in this code.
              context.logger.error(e.error, e.stack);
              throw new ContextualHttpException(
                context,
                "Unknown Error",
                HttpStatus.INTERNAL_SERVER_ERROR,
              );
            }

            throw e;
          }
        } else {
          throw e;
        }
      } else {
        throw e;
      }
    }
  }
}
