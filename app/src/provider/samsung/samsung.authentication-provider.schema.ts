import * as Joi from "joi";
import { AuthenticationProviderTypeEnum } from "@cryptexlabs/authf-data-model";

export const samsungAuthenticationProviderSchema = Joi.object({
  type: Joi.string().valid(AuthenticationProviderTypeEnum.SAMSUNG).required(),
  data: Joi.object({
    clientId: Joi.string().required(),
    userId: Joi.string().required(),
  }).required(),
});
