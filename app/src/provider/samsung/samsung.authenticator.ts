import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import { AuthenticatorInterface } from "../../authenticator/authenticator.interface";
import {
  AuthenticationInterface,
  AuthenticationProviderTypeEnum,
  SamsungAuthenticateInterface,
  SamsungAuthenticationProviderInterface,
  UserInterface,
} from "@cryptexlabs/authf-data-model";
import { Config } from "../../config";
import { SamsungApiUtil } from "./samsung.api.util";
import { HttpException, HttpStatus } from "@nestjs/common";

export class SamsungAuthenticator implements AuthenticatorInterface {
  constructor(
    private readonly data: SamsungAuthenticateInterface,
    private readonly config: Config,
  ) {}

  public async authenticate<T extends UserInterface | void = UserInterface>(
    context: Context,
    authentication: AuthenticationInterface,
  ): Promise<T> {
    const providers: SamsungAuthenticationProviderInterface[] =
      authentication.providers.filter((provider) => {
        return provider.type === AuthenticationProviderTypeEnum.SAMSUNG;
      }) as SamsungAuthenticationProviderInterface[];

    for (const provider of providers) {
      return (await SamsungApiUtil.getUser(
        context,
        provider.data.clientId,
        this.data.data.accessToken,
        provider.data.userId,
      )) as T;
    }

    throw new ContextualHttpException(
      context,
      "Unauthorized",
      HttpStatus.UNAUTHORIZED,
    );
  }
}
