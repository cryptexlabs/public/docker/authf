import { SamsungUserInfoResponseDataInterface } from "./samsung.user-info.response-data.interface";

export class SamsungDisplayName {
  constructor(private readonly profile: SamsungUserInfoResponseDataInterface) {}

  public toString(): string {
    if (this.profile.nickname) {
      return this.profile.nickname;
    }

    if (this.profile.preferred_username) {
      return this.profile.preferred_username;
    }

    if (this.profile.family_name) {
      if (this.profile.given_name) {
        return `${this.profile.given_name} ${this.profile.family_name}`;
      } else {
        return this.profile.family_name;
      }
    }

    if (this.profile.given_name) {
      return this.profile.given_name;
    }

    return "";
  }
}
