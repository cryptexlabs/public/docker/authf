import { IdentifiedProviderInterface } from "../identified-provider.interface";
import { SamsungAuthenticationProviderInterface } from "@cryptexlabs/authf-data-model";

export class SamsungIdentifiedProvider implements IdentifiedProviderInterface {
  constructor(
    public readonly provider: SamsungAuthenticationProviderInterface,
  ) {}

  getIdentifier(): string {
    return this.provider.data.userId;
  }
}
