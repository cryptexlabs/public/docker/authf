import { HttpException, HttpStatus, Inject, Injectable } from "@nestjs/common";
import { Config } from "../../config";
import { OauthHandler } from "../../oauth/oauth.handler";
import {
  Context,
  ContextualHttpException,
  CustomLogger,
} from "@cryptexlabs/codex-nodejs-common";
import {
  AuthenticationProviderTypeEnum,
  SamsungAuthenticationProviderInterface,
} from "@cryptexlabs/authf-data-model";
import { SamsungApiUtil } from "./samsung.api.util";

@Injectable()
export class SamsungService {
  constructor(
    @Inject("CONFIG") private readonly config: Config,
    @Inject("LOGGER") private readonly logger: CustomLogger,
    private readonly oauthHandler: OauthHandler,
  ) {}

  public async handleOauth2Redirect(
    context: Context,
    res: any,
    req: any,
    redirectUri: string,
    code: string,
    clientId: string,
    state: string,
  ) {
    const clientConfig = this.config.samsungOAuthClients.find(
      (item) => item.clientId === clientId,
    );

    if (!clientConfig) {
      throw new ContextualHttpException(
        context,
        `The provided client id: ${clientId} is not configured on this server`,
        HttpStatus.METHOD_NOT_ALLOWED,
      );
    }

    const tokenData = await SamsungApiUtil.getAccessToken(
      context,
      clientConfig,
      code,
      redirectUri,
    );

    const user = await SamsungApiUtil.getUser(
      context,
      clientId,
      tokenData.access_token,
      tokenData.userId,
    );

    const authenticationProvider: SamsungAuthenticationProviderInterface = {
      data: {
        userId: tokenData.userId,
        clientId,
      },
      type: AuthenticationProviderTypeEnum.SAMSUNG,
    };

    await this.oauthHandler.handleOauth(
      context,
      res,
      req,
      user,
      AuthenticationProviderTypeEnum.SAMSUNG,
      "sa-guid",
      tokenData.userId,
      clientConfig.redirectUrl,
      clientConfig.finalRedirectUrl,
      authenticationProvider,
    );
  }
}
