import { Inject, Injectable } from "@nestjs/common";
import { Config } from "../../config";
import {
  AuthenticationProviderTypeEnum,
  SamsungAuthenticationProviderInterface,
} from "@cryptexlabs/authf-data-model";
import { Context } from "@cryptexlabs/codex-nodejs-common";
import { ExampleUser } from "../../example.data";
import { OauthHandler } from "../../oauth/oauth.handler";
import { AuthenticationService } from "../../authentication/authentication.service";

@Injectable()
export class SamsungStubService {
  constructor(
    @Inject("CONFIG") private readonly config: Config,
    private readonly oauthHandler: OauthHandler,
    private readonly authenticationService: AuthenticationService,
  ) {}

  public async handleOauth2Redirect(
    context: Context,
    res: any,
    req: any,
    redirectUri: string,
    code: string,
    clientId: string,
    state: string,
  ) {
    const clientConfig = this.config.samsungOAuthClients.find(
      (item) => item.clientId === clientId,
    );

    const user = new ExampleUser();
    const exampleUserId = "abcde1234";
    const authenticationProvider: SamsungAuthenticationProviderInterface = {
      data: {
        userId: exampleUserId,
        clientId: "38qalsdfs",
      },
      type: AuthenticationProviderTypeEnum.SAMSUNG,
    };
    await this.oauthHandler.handleOauth(
      context,
      res,
      req,
      user,
      AuthenticationProviderTypeEnum.SAMSUNG,
      "sa-guid",
      exampleUserId,
      clientConfig.redirectUrl,
      clientConfig.finalRedirectUrl,
      authenticationProvider,
    );
  }
}
