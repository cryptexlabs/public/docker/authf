import {
  Controller,
  Get,
  Headers,
  Inject,
  Param,
  Query,
  Res,
} from "@nestjs/common";
import { ApiParam, ApiQuery, ApiTags } from "@nestjs/swagger";
import { ContextBuilder } from "@cryptexlabs/codex-nodejs-common";
import { ReAuthService } from "./reauth.service";
import { ReAuthCreateTokenDataInterface } from "@cryptexlabs/authf-data-model";

@Controller("reauth/v1")
@ApiTags("reauth")
export class ReauthController {
  constructor(
    private readonly reauthService: ReAuthService,
    @Inject("CONTEXT_BUILDER") private readonly contextBuilder: ContextBuilder,
  ) {}
  @Get("create-token/:authenticationId")
  @ApiQuery({
    name: "data",
  })
  @ApiParam({
    name: "authenticationId",
  })
  public async authorize(
    @Query("data") data: string,
    @Param("authenticationId") authenticationId: string,
    @Res() res,
    @Headers() headers,
  ) {
    const createTokenData = JSON.parse(
      Buffer.from(data, "base64").toString("utf-8"),
    ) as ReAuthCreateTokenDataInterface;

    const context = this.contextBuilder.build().getResult();

    await this.reauthService.createToken(
      context,
      res,
      authenticationId,
      createTokenData,
    );
  }
}
