import { HttpException, HttpStatus, Injectable } from "@nestjs/common";
import {
  Context,
  ContextualHttpException,
} from "@cryptexlabs/codex-nodejs-common";
import {
  ReAuthCompleteDataInterface,
  ReAuthCreateTokenDataInterface,
} from "@cryptexlabs/authf-data-model";
import { TokenValidator } from "../token";
import * as jwt from "jsonwebtoken";
import { AuthenticationService } from "../authentication/authentication.service";

@Injectable()
export class ReAuthService {
  constructor(
    private readonly tokenValidator: TokenValidator,
    private readonly authenticationService: AuthenticationService,
  ) {}

  public async createToken(
    context: Context,
    res: any,
    authenticationId: string,
    createTokenData: ReAuthCreateTokenDataInterface,
  ) {
    await this.tokenValidator.validateAccessToken(
      context,
      createTokenData.accessToken,
    );
    const decodedAccessToken = jwt.decode(
      createTokenData.accessToken,
    ) as unknown as any;
    const scopes = decodedAccessToken.scopes;
    if (!scopes.includes(`authentication:${authenticationId}:any`)) {
      throw new ContextualHttpException(
        context,
        "Not allowed",
        HttpStatus.FORBIDDEN,
      );
    }

    let authentication;
    do {
      authentication = await this.authenticationService.getAuthentication(
        context,
        authenticationId,
      );
      if (!authentication?.token) {
        context.logger.log(`Waiting for authentication #2`);
        await new Promise((resolve) => setTimeout(resolve, 20));
      }
    } while (!authentication.token);

    if (authentication.token.subject !== createTokenData.userId) {
      throw new ContextualHttpException(
        context,
        "Not allowed",
        HttpStatus.FORBIDDEN,
      );
    }

    const createTokenResponseDataInterface =
      await this.authenticationService.createTokenPairForVerified(
        context,
        authenticationId,
        authentication,
      );

    const compiledExtra = {
      ...(createTokenData.extra || {}),
      ...(createTokenResponseDataInterface.extra || {}),
    };

    const finalExtra =
      Object.keys(compiledExtra).length > 0 ? compiledExtra : undefined;

    const reAuthCompleteData: ReAuthCompleteDataInterface = {
      extra: finalExtra,
      token: createTokenResponseDataInterface.token,
      user: createTokenData.user,
      userId: createTokenData.userId,
    };
    const reAuthCompleteQueryData = Buffer.from(
      JSON.stringify(reAuthCompleteData),
    ).toString("base64");

    res.redirect(
      `${createTokenData.finalRedirectUrl}?data=${reAuthCompleteQueryData}`,
    );
  }
}
