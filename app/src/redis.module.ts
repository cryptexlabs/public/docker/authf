import { Module } from "@nestjs/common";
import { createClient, RedisClientType } from "redis";
import { appConfig } from "./setup";

const { host: redisHost, port: redisPort } = appConfig.redis;

let client: RedisClientType | null;
if (appConfig.redis.enabled) {
  client = createClient({
    socket: {
      port: redisPort,
      host: redisHost,
    },
  });
}

let didRedisConnect = false;
@Module({
  providers: [
    {
      provide: "REDIS",
      useFactory: async () => {
        if (appConfig.redis.enabled) {
          if (!didRedisConnect) {
            didRedisConnect = true;
            await client.connect();
          }
          return client;
        }
        return null;
      },
    },
  ],
  exports: ["REDIS"],
})
export class RedisModule {}
