import * as Joi from "joi";
import { AuthenticateTypeEnum } from "@cryptexlabs/authf-data-model";

export const refreshAuthenticateDataSchema = Joi.object({
  type: Joi.string().valid(AuthenticateTypeEnum.REFRESH).required(),
  data: Joi.object({
    token: Joi.string().required(),
    expiredAccessToken: Joi.string().optional(),
  }).required(),
});
