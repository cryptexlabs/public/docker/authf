import { dirname, basename } from "path";

const rootDir = __dirname;
let appDir = dirname(rootDir);

if (basename(appDir) !== "app") {
  appDir = dirname(appDir);
}
const baseDir = dirname(appDir);

export { rootDir, baseDir, appDir };
