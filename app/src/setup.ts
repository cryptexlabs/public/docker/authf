import { Config } from "./config";
import { appDir, baseDir } from "./root";
import {
  ContextBuilder,
  CustomLogger,
  ServiceClient,
} from "@cryptexlabs/codex-nodejs-common";
import { Logger } from "@nestjs/common";
import { Logger as CustomPinoLogger, PinoLogger } from "nestjs-pino";
import { MessageContext } from "@cryptexlabs/codex-data-model";
import { i18nData, i18nDir } from "./locale/locales";
import { DatabaseTypeEnum } from "./data/database-type.enum";
import { ElasticsearchService } from "@nestjs/elasticsearch";
import { pinoLoggerConfig } from "./logger.config";
import { LoggerService } from "@nestjs/common/services/logger.service";

const appConfig = new Config(
  baseDir,
  appDir,
  "452ECCCF-A223-44DC-B636-3AADF5A3C863",
  "localhost",
);

let baseLoggerImpl: LoggerService = new Logger();

if (appConfig.loggerType.includes("pino")) {
  const pinoLogger = new PinoLogger(pinoLoggerConfig(appConfig));
  baseLoggerImpl = new CustomPinoLogger(pinoLogger, {
    renameContext: null,
  });
}

const baseLogger = new CustomLogger(
  "info",
  appConfig.logLevels,
  false,
  baseLoggerImpl,
);

let elasticsearchService = null;
if (appConfig.dbType === DatabaseTypeEnum.ELASTICSEARCH) {
  elasticsearchService = new ElasticsearchService({
    node: appConfig.elasticsearch.url,
  });
}
const contextBuilder = new ContextBuilder(
  baseLogger,
  appConfig,
  new ServiceClient(appConfig),
  new MessageContext("default", null),
).setI18nDir(i18nDir);

export { appConfig, baseLogger, contextBuilder, elasticsearchService };
