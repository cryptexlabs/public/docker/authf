import { PinoInstrumentation } from "@opentelemetry/instrumentation-pino";
import { NestInstrumentation } from "@opentelemetry/instrumentation-nestjs-core";
import { HttpInstrumentation } from "@opentelemetry/instrumentation-http";
import { ExpressInstrumentation } from "@opentelemetry/instrumentation-express";
import { DnsInstrumentation } from "@opentelemetry/instrumentation-dns";
import { RuntimeNodeInstrumentation } from "@opentelemetry/instrumentation-runtime-node";

import { NodeSDK, NodeSDKConfiguration } from "@opentelemetry/sdk-node";
import { appConfig, baseLogger } from "./setup";

import { BatchSpanProcessor } from "@opentelemetry/sdk-trace-base";

import { Resource } from "@opentelemetry/resources";
import { ATTR_SERVICE_NAME } from "@opentelemetry/semantic-conventions";
import { OTLPTraceExporter } from "@opentelemetry/exporter-trace-otlp-grpc";
import { AWSXRayPropagator } from "@opentelemetry/propagator-aws-xray";
import { AWSXRayIdGenerator } from "@opentelemetry/id-generator-aws-xray";
import { AwsEksDetectorSync } from "@opentelemetry/resource-detector-aws";
import { OnApplicationShutdown } from "@nestjs/common";
import { TraceSampler } from "./tracing-sampler";
import { OpenTelemetryModule } from "nestjs-otel";
import { PrometheusExporter } from "@opentelemetry/exporter-prometheus";
import { AsyncLocalStorageContextManager } from "@opentelemetry/context-async-hooks";
import { TelemetryConfigInterface } from "@cryptexlabs/codex-nodejs-common/lib/src/config/telemetry-config.interface";

const CUSTOM_HEADERS = [
  "x-correlation-id",
  "x-client-version",
  "x-client-id",
  "x-client-name",
  "x-client-variant",
];

const instrumentations = [];
// need to be initialized before nest core module loads
if (process.env.TELEMETRY_TRACE_ENABLED === "true") {
  instrumentations.push(
    new PinoInstrumentation(),
    new NestInstrumentation(),
    new HttpInstrumentation({
      ignoreIncomingRequestHook: (req) => {
        return (
          req.url.includes("healthz") ||
          req.url === "/metrics" ||
          req.url.includes("docs")
        );
      },
      headersToSpanAttributes: {
        client: {
          requestHeaders: CUSTOM_HEADERS,
        },
        server: {
          requestHeaders: CUSTOM_HEADERS,
        },
      },
    }),
    new ExpressInstrumentation(),
    new DnsInstrumentation(),
    new RuntimeNodeInstrumentation({
      eventLoopUtilizationMeasurementInterval: 5000,
    }),
  );
}

const opentelemetryModuleImport = OpenTelemetryModule.forRoot({
  metrics: {
    hostMetrics: false, // Includes Host Metrics
    apiMetrics: {
      enable: true, // Includes api metrics
      defaultAttributes: {
        service_name: appConfig.serviceName,
      },
      ignoreRoutes: ["/favicon.ico", "/healthz"],
      ignoreUndefinedRoutes: false, // Records metrics for all URLs, even undefined ones
    },
  },
});

export const OpenTelemetryModuleImport = opentelemetryModuleImport;
export class Telemetry implements OnApplicationShutdown {
  private _openTelemetrySDK: NodeSDK;
  private _sdkOptions: Partial<NodeSDKConfiguration> = {};
  private _otlpExporter: OTLPTraceExporter;
  private _telemetryConfig: TelemetryConfigInterface = appConfig.telemetry;

  constructor() {
    if (this._telemetryConfig.metrics.enabled) {
      const metricReader = new PrometheusExporter({
        port: this._telemetryConfig.metrics.port,
        endpoint: this._telemetryConfig.metrics.path,
      });
      this._sdkOptions.metricReader = metricReader;
    }
    if (this._telemetryConfig.traces.enabled) {
      this._otlpExporter = new OTLPTraceExporter({
        url: this._telemetryConfig.traces.traceUrl,
      });
      this._sdkOptions.traceExporter = this._otlpExporter;
      this._sdkOptions.idGenerator = new AWSXRayIdGenerator();
      this._sdkOptions.instrumentations = instrumentations;
      this._sdkOptions.sampler = new TraceSampler();

      this._sdkOptions.spanProcessors = [
        new BatchSpanProcessor(this._otlpExporter),
      ];
      this._sdkOptions.textMapPropagator = new AWSXRayPropagator();
    }

    this._sdkOptions.contextManager = new AsyncLocalStorageContextManager();
    this._sdkOptions.resource = Resource.default().merge(
      new Resource({
        [ATTR_SERVICE_NAME]: appConfig.serviceName,
      }),
    );
  }

  async start() {
    if (this._telemetryConfig.traces.enabled) {
      const eksDetector = new AwsEksDetectorSync().detect();
      this._sdkOptions.resource.merge(eksDetector);
    }
    this._openTelemetrySDK = new NodeSDK(this._sdkOptions);
    this._openTelemetrySDK.start();
  }

  onApplicationShutdown(signal: string) {
    baseLogger.debug(`received app shutdown signal: ${signal}`); // e.g. "SIGINT"
    this._openTelemetrySDK
      .shutdown()
      .then(() => baseLogger.debug("telemetry sdk shutdown successfully"))
      .catch((err) =>
        baseLogger.error("error shutting down telemetry SDK", err),
      );
  }
}
