import { AuthenticationTokenDataInterface } from "@cryptexlabs/authf-data-model";

export class TokenPairData {
  constructor(private readonly tokenData: AuthenticationTokenDataInterface) {}

  getAccessTokenExpiresIn() {
    return this.tokenData.expirationPolicy.access.length;
  }

  getRefreshTokenExpiresIn() {
    return this.tokenData.expirationPolicy.refresh.length;
  }

  getAccessTokenPayload() {
    return {
      sub: this.tokenData.subject,
      type: "access",
      ...this.tokenData.body,
    };
  }

  getRefreshTokenPayload(authenticationId: string) {
    return { sub: authenticationId, type: "refresh" };
  }
}
