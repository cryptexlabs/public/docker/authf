import { Attributes, Context, Link, SpanKind } from "@opentelemetry/api";
import {
  Sampler,
  SamplingDecision,
  SamplingResult,
} from "@opentelemetry/sdk-trace-base";

export class TraceSampler implements Sampler {
  shouldSample(
    context: Context,
    traceId: string,
    spanName: string,
    spanKind: SpanKind,
    attributes: Attributes,
    links: Link[],
  ): SamplingResult {
    if (
      spanName === "HealthzController.root" ||
      attributes["db.operation"] === "client.ping" ||
      attributes["db.statement"] === "SELECT 1" ||
      attributes["http.route"] === "/healthz" ||
      attributes["nestjs.callback"] === "root"
    ) {
      return {
        decision: SamplingDecision.NOT_RECORD,
      };
    } else {
      return {
        decision: SamplingDecision.RECORD_AND_SAMPLED,
        attributes,
      };
    }
  }
  toString(): string {
    return "Trace sampler for filtering noisy samples";
  }
}
