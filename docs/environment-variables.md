# Authf Environment Variables

### Base environment variables
| Name | Description | Optionality | Default | Example(s) | Options |
| ---- | ----------- | ----------- | ------- | ------- | ------- |
| `LOG_LEVELS` | Comma separated string of log levels to be included. | optional | `debug,info,error` | `debug,info,error` | `debug` `info` `warn` `error` |
| `HTTP_PORT` |  Port to serve REST API and swagger docs on |  optional | `3000` | `3000` |
| `DOCS_PREFIX` |  Prefix where the Swagger docs are hosted on eg `/docs/{api version}` | optional | docs` | `docs`, `swagger` |
| `API_VERSION` |  API version. Should generally not be changed. |  optional | `v1` | `v1`, `v2`, `v3` | |
| `APP_PREFIX` | Prefix in url to REST API eg. `/api/{api version}` | optional | `api` |  `api`  |
| `ENV_NAME` | Name of the environment | optional | `localhost` | `developmenet`, `docker`, `localhost`, `production` |  |
| `JWT_PRIVATE_KEY_PATH` | Path to the private key used to generate JWT tokens | optional | localhost: `./app/config/jwt-rs256.key`, all other environments: `/var/app/config/jwt-rs256.key` | `/var/app/config/jwt-rs256.key` | 
| `JWT_PUBLIC_KEY_PATH` | Path to the public key used to generate JWT tokens | optional | localhost: `./app/config/jwt-rs256.key`, all other environments: `/var/app/config/jwt-rs256.pub`  | `/var/app/config/jwt-rs256.pub` | 
| `JWT_ALGORITHM` | Algorithm to sign JWT tokens with | optional | `RS256` | `RS256` | `HS256`, `HS384`, `HS512`, `RS256`, `RS384`, `RS512`, `PS256`, `PS384`, `PS512`, `ES256`, `ES384`, `ES512` |
| `DATABASE_TYPE` | The type of database to use for authf |  optional | `elasticsearch` |  `elasticsearch` | `elasticsearch`, `mysql` |
| `AUTHF_SERVER_CONFIG_PATH` | Path to [Auth Configuration File](./server-config.md) |  optional | localhost: `./app/config/default-config.yaml`, all other environments: |  `/var/app/config/config.yaml` |  |

## Elasticsearch environment variables
| Name | Description | Optionality | Default | Example(s) | Options |
| ---- | ----------- | ----------- | ------- | ------- | ------- |
| `ELASTICSEARCH_URL` | The protocol, hostname, and port of the elasticsearch database server | optional | `http:/elasticsearch:9200` |  `http://127.0.0.1:9200` | |

## MySQL environment variables
| Name | Description | Optionality | Default | Example(s) | Options |
| ---- | ----------- | ----------- | ------- | ------- | ------- |
| `SQL_CLIENT` | SQL client type |  required for `DATABASE_TYPE=mysql` |  |  `mysql2` | `mysql2` |
| `SQL_HOSTNAME` |  Hostname to sql database server |  required for `DATABASE_TYPE=mysql` |  |  `mysql-dev.myapp.com` | |
| `SQL_PORT` |  Port to sql database server |  required for `DATABASE_TYPE=mysql` |  |  `3306` | |
| `SQL_USERNAME` |  Username to sql database |  required for `DATABASE_TYPE=mysql` |  | `auth` | |
| `SQL_PASSWORD` |  Password to sql database |  required for `DATABASE_TYPE=mysql` |  | `password` | |
| `SQL_DATABASE` |  The name of the sql database |  required for `DATABASE_TYPE=mysql` |  | `authf` | |
| `SQL_POOL_MIN` |  The minimum number of connections to open to database |  required for `DATABASE_TYPE=mysql` |  |  `2` | |
| `SQL_POOL_MAX` |  The maximum number of connections to open to database |  required for `DATABASE_TYPE=mysql` |  |  `10` | |
| `SQL_HOSTNAME_RO` |  Hostname to mysql database with read-only access. Use same setting as `SQL_HOSTNAME` if no read-only connection is available  |  required for `DATABASE_TYPE=mysql` |  |  `mysql-read-only-dev.myapp.com` | |
| `SQL_PORT_RO` |  Port to read-only mysql database. Use same setting as `SQL_PORT` if no read-only connection is available | required for `DATABASE_TYPE=mysql` |  |  `3306` | |
| `SQL_POOL_RO_MIN` | The minimum number of connections to open to read-only database. Use same setting as `SQL_POOL_MIN` if no read-only connection is available |  required for `DATABASE_TYPE=mysql` |  | `2` |  |
| `SQL_POOL_RO_MAX` | The minimum number of connections to open to read-only database. Use same setting as `SQL_POOL_MAX` if no read-only connection is available |  required for `DATABASE_TYPE=mysql` |  |  `10` |  |

Example `.env` configuration
```dotenv
LOG_LEVELS=debug,info,error
HTTP_PORT=3000
DOCS_PREFIX=docs
API_VERSION=v1
APP_PREFIX=api
ENV_NAME=localhost

JWT_PRIVATE_KEY_PATH=myprivatekey.pem
JWT_PUBLIC_KEY_PATH=mypublickey.pem
JWT_ALGORITHM=RS256
DATABASE_TYPE=elasticsearch
ELASTICSEARCH_URL=http://127.0.0.1:9200
ADMIN_USERNAME=admin
ADMIN_PASSWORD=password

# Master DB
SQL_CLIENT=mysql2
SQL_HOSTNAME=mysql
SQL_PORT=3306
SQL_USERNAME=root
SQL_PASSWORD=password
SQL_DATABASE=authf
SQL_POOL_MIN=2
SQL_POOL_MAX=10

# Read Only DB
SQL_HOSTNAME_RO=mysql
SQL_PORT_RO=3306
SQL_POOL_RO_MIN=2
SQL_POOL_RO_MAX=10
```