# Server Config file

Example Configuration
```yaml
# Optional OAuth configurations
oAuth:
  # Optional Samsung OAuth configurations
  samsung:
    # Required the client id for the samsung OAuth client
    - clientId: asdf
      # Required the client secret for the samsung OAuth client
      clientSecret: blahblahblah
      # Required a URL to redirect to after connecting to Samsung API
      redirectUrl: http://iamservice.com
```